﻿<?xml version='1.0' encoding='UTF-8'?>
<Project Type="Project" LVVersion="13008000">
	<Item Name="My Computer" Type="My Computer">
		<Property Name="server.app.propertiesEnabled" Type="Bool">true</Property>
		<Property Name="server.control.propertiesEnabled" Type="Bool">true</Property>
		<Property Name="server.tcp.enabled" Type="Bool">false</Property>
		<Property Name="server.tcp.port" Type="Int">0</Property>
		<Property Name="server.tcp.serviceName" Type="Str">My Computer/VI Server</Property>
		<Property Name="server.tcp.serviceName.default" Type="Str">My Computer/VI Server</Property>
		<Property Name="server.vi.callsEnabled" Type="Bool">true</Property>
		<Property Name="server.vi.propertiesEnabled" Type="Bool">true</Property>
		<Property Name="specify.custom.address" Type="Bool">false</Property>
		<Item Name="Main.vi" Type="VI" URL="../Main.vi"/>
		<Item Name="Dependencies" Type="Dependencies">
			<Item Name="vi.lib" Type="Folder">
				<Item Name="Beep.vi" Type="VI" URL="/&lt;vilib&gt;/Platform/system.llb/Beep.vi"/>
				<Item Name="Bit-array To Byte-array.vi" Type="VI" URL="/&lt;vilib&gt;/picture/pictutil.llb/Bit-array To Byte-array.vi"/>
				<Item Name="BuildHelpPath.vi" Type="VI" URL="/&lt;vilib&gt;/Utility/error.llb/BuildHelpPath.vi"/>
				<Item Name="Built App File Layout.vi" Type="VI" URL="/&lt;vilib&gt;/AppBuilder/Built App File Layout.vi"/>
				<Item Name="Bytes At Serial Port.vi" Type="VI" URL="/&lt;vilib&gt;/Instr/serial.llb/Bytes At Serial Port.vi"/>
				<Item Name="Calc Long Word Padded Width.vi" Type="VI" URL="/&lt;vilib&gt;/picture/bmp.llb/Calc Long Word Padded Width.vi"/>
				<Item Name="Check Color Table Size.vi" Type="VI" URL="/&lt;vilib&gt;/picture/jpeg.llb/Check Color Table Size.vi"/>
				<Item Name="Check Data Size.vi" Type="VI" URL="/&lt;vilib&gt;/picture/jpeg.llb/Check Data Size.vi"/>
				<Item Name="Check File Permissions.vi" Type="VI" URL="/&lt;vilib&gt;/picture/jpeg.llb/Check File Permissions.vi"/>
				<Item Name="Check if File or Folder Exists.vi" Type="VI" URL="/&lt;vilib&gt;/Utility/libraryn.llb/Check if File or Folder Exists.vi"/>
				<Item Name="Check Path.vi" Type="VI" URL="/&lt;vilib&gt;/picture/jpeg.llb/Check Path.vi"/>
				<Item Name="Check Special Tags.vi" Type="VI" URL="/&lt;vilib&gt;/Utility/error.llb/Check Special Tags.vi"/>
				<Item Name="Clear Errors.vi" Type="VI" URL="/&lt;vilib&gt;/Utility/error.llb/Clear Errors.vi"/>
				<Item Name="Close File+.vi" Type="VI" URL="/&lt;vilib&gt;/Utility/file.llb/Close File+.vi"/>
				<Item Name="Close Registry Key.vi" Type="VI" URL="/&lt;vilib&gt;/registry/registry.llb/Close Registry Key.vi"/>
				<Item Name="Color to RGB.vi" Type="VI" URL="/&lt;vilib&gt;/Utility/colorconv.llb/Color to RGB.vi"/>
				<Item Name="compatCalcOffset.vi" Type="VI" URL="/&lt;vilib&gt;/_oldvers/_oldvers.llb/compatCalcOffset.vi"/>
				<Item Name="compatFileDialog.vi" Type="VI" URL="/&lt;vilib&gt;/_oldvers/_oldvers.llb/compatFileDialog.vi"/>
				<Item Name="compatOpenFileOperation.vi" Type="VI" URL="/&lt;vilib&gt;/_oldvers/_oldvers.llb/compatOpenFileOperation.vi"/>
				<Item Name="compatOverwrite.vi" Type="VI" URL="/&lt;vilib&gt;/_oldvers/_oldvers.llb/compatOverwrite.vi"/>
				<Item Name="compatReadText.vi" Type="VI" URL="/&lt;vilib&gt;/_oldvers/_oldvers.llb/compatReadText.vi"/>
				<Item Name="compatWriteText.vi" Type="VI" URL="/&lt;vilib&gt;/_oldvers/_oldvers.llb/compatWriteText.vi"/>
				<Item Name="Convert property node font to graphics font.vi" Type="VI" URL="/&lt;vilib&gt;/Utility/error.llb/Convert property node font to graphics font.vi"/>
				<Item Name="Create ActiveX Event Queue.vi" Type="VI" URL="/&lt;vilib&gt;/Platform/ax-events.llb/Create ActiveX Event Queue.vi"/>
				<Item Name="Create Error Clust.vi" Type="VI" URL="/&lt;vilib&gt;/Platform/ax-events.llb/Create Error Clust.vi"/>
				<Item Name="Create Mask By Alpha.vi" Type="VI" URL="/&lt;vilib&gt;/picture/picture.llb/Create Mask By Alpha.vi"/>
				<Item Name="DAQmx Adjust E-Series Calibration.vi" Type="VI" URL="/&lt;vilib&gt;/DAQmx/calibration/calibrationInfo.llb/DAQmx Adjust E-Series Calibration.vi"/>
				<Item Name="DAQmx Clear Task.vi" Type="VI" URL="/&lt;vilib&gt;/DAQmx/configure/task.llb/DAQmx Clear Task.vi"/>
				<Item Name="DAQmx Close External Calibration.vi" Type="VI" URL="/&lt;vilib&gt;/DAQmx/calibration/calibrationInfo.llb/DAQmx Close External Calibration.vi"/>
				<Item Name="DAQmx Create AI Channel (sub).vi" Type="VI" URL="/&lt;vilib&gt;/DAQmx/create/channels.llb/DAQmx Create AI Channel (sub).vi"/>
				<Item Name="DAQmx Create AI Channel TEDS(sub).vi" Type="VI" URL="/&lt;vilib&gt;/DAQmx/create/channels.llb/DAQmx Create AI Channel TEDS(sub).vi"/>
				<Item Name="DAQmx Create AO Channel (sub).vi" Type="VI" URL="/&lt;vilib&gt;/DAQmx/create/channels.llb/DAQmx Create AO Channel (sub).vi"/>
				<Item Name="DAQmx Create Channel (AI-Acceleration-Accelerometer).vi" Type="VI" URL="/&lt;vilib&gt;/DAQmx/create/channels.llb/DAQmx Create Channel (AI-Acceleration-Accelerometer).vi"/>
				<Item Name="DAQmx Create Channel (AI-Bridge).vi" Type="VI" URL="/&lt;vilib&gt;/DAQmx/create/channels.llb/DAQmx Create Channel (AI-Bridge).vi"/>
				<Item Name="DAQmx Create Channel (AI-Current-Basic).vi" Type="VI" URL="/&lt;vilib&gt;/DAQmx/create/channels.llb/DAQmx Create Channel (AI-Current-Basic).vi"/>
				<Item Name="DAQmx Create Channel (AI-Current-RMS).vi" Type="VI" URL="/&lt;vilib&gt;/DAQmx/create/channels.llb/DAQmx Create Channel (AI-Current-RMS).vi"/>
				<Item Name="DAQmx Create Channel (AI-Force-Bridge-Polynomial).vi" Type="VI" URL="/&lt;vilib&gt;/DAQmx/create/channels.llb/DAQmx Create Channel (AI-Force-Bridge-Polynomial).vi"/>
				<Item Name="DAQmx Create Channel (AI-Force-Bridge-Table).vi" Type="VI" URL="/&lt;vilib&gt;/DAQmx/create/channels.llb/DAQmx Create Channel (AI-Force-Bridge-Table).vi"/>
				<Item Name="DAQmx Create Channel (AI-Force-Bridge-Two-Point-Linear).vi" Type="VI" URL="/&lt;vilib&gt;/DAQmx/create/channels.llb/DAQmx Create Channel (AI-Force-Bridge-Two-Point-Linear).vi"/>
				<Item Name="DAQmx Create Channel (AI-Force-IEPE Sensor).vi" Type="VI" URL="/&lt;vilib&gt;/DAQmx/create/channels.llb/DAQmx Create Channel (AI-Force-IEPE Sensor).vi"/>
				<Item Name="DAQmx Create Channel (AI-Frequency-Voltage).vi" Type="VI" URL="/&lt;vilib&gt;/DAQmx/create/channels.llb/DAQmx Create Channel (AI-Frequency-Voltage).vi"/>
				<Item Name="DAQmx Create Channel (AI-Position-EddyCurrentProxProbe).vi" Type="VI" URL="/&lt;vilib&gt;/DAQmx/create/channels.llb/DAQmx Create Channel (AI-Position-EddyCurrentProxProbe).vi"/>
				<Item Name="DAQmx Create Channel (AI-Position-LVDT).vi" Type="VI" URL="/&lt;vilib&gt;/DAQmx/create/channels.llb/DAQmx Create Channel (AI-Position-LVDT).vi"/>
				<Item Name="DAQmx Create Channel (AI-Position-RVDT).vi" Type="VI" URL="/&lt;vilib&gt;/DAQmx/create/channels.llb/DAQmx Create Channel (AI-Position-RVDT).vi"/>
				<Item Name="DAQmx Create Channel (AI-Pressure-Bridge-Polynomial).vi" Type="VI" URL="/&lt;vilib&gt;/DAQmx/create/channels.llb/DAQmx Create Channel (AI-Pressure-Bridge-Polynomial).vi"/>
				<Item Name="DAQmx Create Channel (AI-Pressure-Bridge-Table).vi" Type="VI" URL="/&lt;vilib&gt;/DAQmx/create/channels.llb/DAQmx Create Channel (AI-Pressure-Bridge-Table).vi"/>
				<Item Name="DAQmx Create Channel (AI-Pressure-Bridge-Two-Point-Linear).vi" Type="VI" URL="/&lt;vilib&gt;/DAQmx/create/channels.llb/DAQmx Create Channel (AI-Pressure-Bridge-Two-Point-Linear).vi"/>
				<Item Name="DAQmx Create Channel (AI-Resistance).vi" Type="VI" URL="/&lt;vilib&gt;/DAQmx/create/channels.llb/DAQmx Create Channel (AI-Resistance).vi"/>
				<Item Name="DAQmx Create Channel (AI-Sound Pressure-Microphone).vi" Type="VI" URL="/&lt;vilib&gt;/DAQmx/create/channels.llb/DAQmx Create Channel (AI-Sound Pressure-Microphone).vi"/>
				<Item Name="DAQmx Create Channel (AI-Strain-Rosette Strain Gage).vi" Type="VI" URL="/&lt;vilib&gt;/DAQmx/create/channels.llb/DAQmx Create Channel (AI-Strain-Rosette Strain Gage).vi"/>
				<Item Name="DAQmx Create Channel (AI-Strain-Strain Gage).vi" Type="VI" URL="/&lt;vilib&gt;/DAQmx/create/channels.llb/DAQmx Create Channel (AI-Strain-Strain Gage).vi"/>
				<Item Name="DAQmx Create Channel (AI-Temperature-Built-in Sensor).vi" Type="VI" URL="/&lt;vilib&gt;/DAQmx/create/channels.llb/DAQmx Create Channel (AI-Temperature-Built-in Sensor).vi"/>
				<Item Name="DAQmx Create Channel (AI-Temperature-RTD).vi" Type="VI" URL="/&lt;vilib&gt;/DAQmx/create/channels.llb/DAQmx Create Channel (AI-Temperature-RTD).vi"/>
				<Item Name="DAQmx Create Channel (AI-Temperature-Thermistor-Iex).vi" Type="VI" URL="/&lt;vilib&gt;/DAQmx/create/channels.llb/DAQmx Create Channel (AI-Temperature-Thermistor-Iex).vi"/>
				<Item Name="DAQmx Create Channel (AI-Temperature-Thermistor-Vex).vi" Type="VI" URL="/&lt;vilib&gt;/DAQmx/create/channels.llb/DAQmx Create Channel (AI-Temperature-Thermistor-Vex).vi"/>
				<Item Name="DAQmx Create Channel (AI-Temperature-Thermocouple).vi" Type="VI" URL="/&lt;vilib&gt;/DAQmx/create/channels.llb/DAQmx Create Channel (AI-Temperature-Thermocouple).vi"/>
				<Item Name="DAQmx Create Channel (AI-Torque-Bridge-Polynomial).vi" Type="VI" URL="/&lt;vilib&gt;/DAQmx/create/channels.llb/DAQmx Create Channel (AI-Torque-Bridge-Polynomial).vi"/>
				<Item Name="DAQmx Create Channel (AI-Torque-Bridge-Table).vi" Type="VI" URL="/&lt;vilib&gt;/DAQmx/create/channels.llb/DAQmx Create Channel (AI-Torque-Bridge-Table).vi"/>
				<Item Name="DAQmx Create Channel (AI-Torque-Bridge-Two-Point-Linear).vi" Type="VI" URL="/&lt;vilib&gt;/DAQmx/create/channels.llb/DAQmx Create Channel (AI-Torque-Bridge-Two-Point-Linear).vi"/>
				<Item Name="DAQmx Create Channel (AI-Velocity-IEPE Sensor).vi" Type="VI" URL="/&lt;vilib&gt;/DAQmx/create/channels.llb/DAQmx Create Channel (AI-Velocity-IEPE Sensor).vi"/>
				<Item Name="DAQmx Create Channel (AI-Voltage-Basic).vi" Type="VI" URL="/&lt;vilib&gt;/DAQmx/create/channels.llb/DAQmx Create Channel (AI-Voltage-Basic).vi"/>
				<Item Name="DAQmx Create Channel (AI-Voltage-Custom with Excitation).vi" Type="VI" URL="/&lt;vilib&gt;/DAQmx/create/channels.llb/DAQmx Create Channel (AI-Voltage-Custom with Excitation).vi"/>
				<Item Name="DAQmx Create Channel (AI-Voltage-RMS).vi" Type="VI" URL="/&lt;vilib&gt;/DAQmx/create/channels.llb/DAQmx Create Channel (AI-Voltage-RMS).vi"/>
				<Item Name="DAQmx Create Channel (AO-Current-Basic).vi" Type="VI" URL="/&lt;vilib&gt;/DAQmx/create/channels.llb/DAQmx Create Channel (AO-Current-Basic).vi"/>
				<Item Name="DAQmx Create Channel (AO-FuncGen).vi" Type="VI" URL="/&lt;vilib&gt;/DAQmx/create/channels.llb/DAQmx Create Channel (AO-FuncGen).vi"/>
				<Item Name="DAQmx Create Channel (AO-Voltage-Basic).vi" Type="VI" URL="/&lt;vilib&gt;/DAQmx/create/channels.llb/DAQmx Create Channel (AO-Voltage-Basic).vi"/>
				<Item Name="DAQmx Create Channel (CI-Count Edges).vi" Type="VI" URL="/&lt;vilib&gt;/DAQmx/create/channels.llb/DAQmx Create Channel (CI-Count Edges).vi"/>
				<Item Name="DAQmx Create Channel (CI-Frequency).vi" Type="VI" URL="/&lt;vilib&gt;/DAQmx/create/channels.llb/DAQmx Create Channel (CI-Frequency).vi"/>
				<Item Name="DAQmx Create Channel (CI-GPS Timestamp).vi" Type="VI" URL="/&lt;vilib&gt;/DAQmx/create/channels.llb/DAQmx Create Channel (CI-GPS Timestamp).vi"/>
				<Item Name="DAQmx Create Channel (CI-Period).vi" Type="VI" URL="/&lt;vilib&gt;/DAQmx/create/channels.llb/DAQmx Create Channel (CI-Period).vi"/>
				<Item Name="DAQmx Create Channel (CI-Position-Angular Encoder).vi" Type="VI" URL="/&lt;vilib&gt;/DAQmx/create/channels.llb/DAQmx Create Channel (CI-Position-Angular Encoder).vi"/>
				<Item Name="DAQmx Create Channel (CI-Position-Linear Encoder).vi" Type="VI" URL="/&lt;vilib&gt;/DAQmx/create/channels.llb/DAQmx Create Channel (CI-Position-Linear Encoder).vi"/>
				<Item Name="DAQmx Create Channel (CI-Pulse Freq).vi" Type="VI" URL="/&lt;vilib&gt;/DAQmx/create/channels.llb/DAQmx Create Channel (CI-Pulse Freq).vi"/>
				<Item Name="DAQmx Create Channel (CI-Pulse Ticks).vi" Type="VI" URL="/&lt;vilib&gt;/DAQmx/create/channels.llb/DAQmx Create Channel (CI-Pulse Ticks).vi"/>
				<Item Name="DAQmx Create Channel (CI-Pulse Time).vi" Type="VI" URL="/&lt;vilib&gt;/DAQmx/create/channels.llb/DAQmx Create Channel (CI-Pulse Time).vi"/>
				<Item Name="DAQmx Create Channel (CI-Pulse Width).vi" Type="VI" URL="/&lt;vilib&gt;/DAQmx/create/channels.llb/DAQmx Create Channel (CI-Pulse Width).vi"/>
				<Item Name="DAQmx Create Channel (CI-Semi Period).vi" Type="VI" URL="/&lt;vilib&gt;/DAQmx/create/channels.llb/DAQmx Create Channel (CI-Semi Period).vi"/>
				<Item Name="DAQmx Create Channel (CI-Two Edge Separation).vi" Type="VI" URL="/&lt;vilib&gt;/DAQmx/create/channels.llb/DAQmx Create Channel (CI-Two Edge Separation).vi"/>
				<Item Name="DAQmx Create Channel (CO-Pulse Generation-Frequency).vi" Type="VI" URL="/&lt;vilib&gt;/DAQmx/create/channels.llb/DAQmx Create Channel (CO-Pulse Generation-Frequency).vi"/>
				<Item Name="DAQmx Create Channel (CO-Pulse Generation-Ticks).vi" Type="VI" URL="/&lt;vilib&gt;/DAQmx/create/channels.llb/DAQmx Create Channel (CO-Pulse Generation-Ticks).vi"/>
				<Item Name="DAQmx Create Channel (CO-Pulse Generation-Time).vi" Type="VI" URL="/&lt;vilib&gt;/DAQmx/create/channels.llb/DAQmx Create Channel (CO-Pulse Generation-Time).vi"/>
				<Item Name="DAQmx Create Channel (DI-Digital Input).vi" Type="VI" URL="/&lt;vilib&gt;/DAQmx/create/channels.llb/DAQmx Create Channel (DI-Digital Input).vi"/>
				<Item Name="DAQmx Create Channel (DO-Digital Output).vi" Type="VI" URL="/&lt;vilib&gt;/DAQmx/create/channels.llb/DAQmx Create Channel (DO-Digital Output).vi"/>
				<Item Name="DAQmx Create Channel (TEDS-AI-Acceleration-Accelerometer).vi" Type="VI" URL="/&lt;vilib&gt;/DAQmx/create/channels.llb/DAQmx Create Channel (TEDS-AI-Acceleration-Accelerometer).vi"/>
				<Item Name="DAQmx Create Channel (TEDS-AI-Bridge).vi" Type="VI" URL="/&lt;vilib&gt;/DAQmx/create/channels.llb/DAQmx Create Channel (TEDS-AI-Bridge).vi"/>
				<Item Name="DAQmx Create Channel (TEDS-AI-Current-Basic).vi" Type="VI" URL="/&lt;vilib&gt;/DAQmx/create/channels.llb/DAQmx Create Channel (TEDS-AI-Current-Basic).vi"/>
				<Item Name="DAQmx Create Channel (TEDS-AI-Force-Bridge).vi" Type="VI" URL="/&lt;vilib&gt;/DAQmx/create/channels.llb/DAQmx Create Channel (TEDS-AI-Force-Bridge).vi"/>
				<Item Name="DAQmx Create Channel (TEDS-AI-Force-IEPE Sensor).vi" Type="VI" URL="/&lt;vilib&gt;/DAQmx/create/channels.llb/DAQmx Create Channel (TEDS-AI-Force-IEPE Sensor).vi"/>
				<Item Name="DAQmx Create Channel (TEDS-AI-Position-LVDT).vi" Type="VI" URL="/&lt;vilib&gt;/DAQmx/create/channels.llb/DAQmx Create Channel (TEDS-AI-Position-LVDT).vi"/>
				<Item Name="DAQmx Create Channel (TEDS-AI-Position-RVDT).vi" Type="VI" URL="/&lt;vilib&gt;/DAQmx/create/channels.llb/DAQmx Create Channel (TEDS-AI-Position-RVDT).vi"/>
				<Item Name="DAQmx Create Channel (TEDS-AI-Pressure-Bridge).vi" Type="VI" URL="/&lt;vilib&gt;/DAQmx/create/channels.llb/DAQmx Create Channel (TEDS-AI-Pressure-Bridge).vi"/>
				<Item Name="DAQmx Create Channel (TEDS-AI-Resistance).vi" Type="VI" URL="/&lt;vilib&gt;/DAQmx/create/channels.llb/DAQmx Create Channel (TEDS-AI-Resistance).vi"/>
				<Item Name="DAQmx Create Channel (TEDS-AI-Sound Pressure-Microphone).vi" Type="VI" URL="/&lt;vilib&gt;/DAQmx/create/channels.llb/DAQmx Create Channel (TEDS-AI-Sound Pressure-Microphone).vi"/>
				<Item Name="DAQmx Create Channel (TEDS-AI-Strain-Strain Gage).vi" Type="VI" URL="/&lt;vilib&gt;/DAQmx/create/channels.llb/DAQmx Create Channel (TEDS-AI-Strain-Strain Gage).vi"/>
				<Item Name="DAQmx Create Channel (TEDS-AI-Temperature-RTD).vi" Type="VI" URL="/&lt;vilib&gt;/DAQmx/create/channels.llb/DAQmx Create Channel (TEDS-AI-Temperature-RTD).vi"/>
				<Item Name="DAQmx Create Channel (TEDS-AI-Temperature-Thermistor-Iex).vi" Type="VI" URL="/&lt;vilib&gt;/DAQmx/create/channels.llb/DAQmx Create Channel (TEDS-AI-Temperature-Thermistor-Iex).vi"/>
				<Item Name="DAQmx Create Channel (TEDS-AI-Temperature-Thermistor-Vex).vi" Type="VI" URL="/&lt;vilib&gt;/DAQmx/create/channels.llb/DAQmx Create Channel (TEDS-AI-Temperature-Thermistor-Vex).vi"/>
				<Item Name="DAQmx Create Channel (TEDS-AI-Temperature-Thermocouple).vi" Type="VI" URL="/&lt;vilib&gt;/DAQmx/create/channels.llb/DAQmx Create Channel (TEDS-AI-Temperature-Thermocouple).vi"/>
				<Item Name="DAQmx Create Channel (TEDS-AI-Torque-Bridge).vi" Type="VI" URL="/&lt;vilib&gt;/DAQmx/create/channels.llb/DAQmx Create Channel (TEDS-AI-Torque-Bridge).vi"/>
				<Item Name="DAQmx Create Channel (TEDS-AI-Voltage-Basic).vi" Type="VI" URL="/&lt;vilib&gt;/DAQmx/create/channels.llb/DAQmx Create Channel (TEDS-AI-Voltage-Basic).vi"/>
				<Item Name="DAQmx Create Channel (TEDS-AI-Voltage-Custom with Excitation).vi" Type="VI" URL="/&lt;vilib&gt;/DAQmx/create/channels.llb/DAQmx Create Channel (TEDS-AI-Voltage-Custom with Excitation).vi"/>
				<Item Name="DAQmx Create CI Channel (sub).vi" Type="VI" URL="/&lt;vilib&gt;/DAQmx/create/channels.llb/DAQmx Create CI Channel (sub).vi"/>
				<Item Name="DAQmx Create CO Channel (sub).vi" Type="VI" URL="/&lt;vilib&gt;/DAQmx/create/channels.llb/DAQmx Create CO Channel (sub).vi"/>
				<Item Name="DAQmx Create DI Channel (sub).vi" Type="VI" URL="/&lt;vilib&gt;/DAQmx/create/channels.llb/DAQmx Create DI Channel (sub).vi"/>
				<Item Name="DAQmx Create DO Channel (sub).vi" Type="VI" URL="/&lt;vilib&gt;/DAQmx/create/channels.llb/DAQmx Create DO Channel (sub).vi"/>
				<Item Name="DAQmx Create Strain Rosette AI Channels (sub).vi" Type="VI" URL="/&lt;vilib&gt;/DAQmx/create/channels.llb/DAQmx Create Strain Rosette AI Channels (sub).vi"/>
				<Item Name="DAQmx Create Virtual Channel.vi" Type="VI" URL="/&lt;vilib&gt;/DAQmx/create/channels.llb/DAQmx Create Virtual Channel.vi"/>
				<Item Name="DAQmx Fill In Error Info.vi" Type="VI" URL="/&lt;vilib&gt;/DAQmx/miscellaneous.llb/DAQmx Fill In Error Info.vi"/>
				<Item Name="DAQmx Initialize External Calibration.vi" Type="VI" URL="/&lt;vilib&gt;/DAQmx/calibration/calibrationInfo.llb/DAQmx Initialize External Calibration.vi"/>
				<Item Name="DAQmx Read (Analog 1D DBL 1Chan NSamp).vi" Type="VI" URL="/&lt;vilib&gt;/DAQmx/read.llb/DAQmx Read (Analog 1D DBL 1Chan NSamp).vi"/>
				<Item Name="DAQmx Read (Analog 1D DBL NChan 1Samp).vi" Type="VI" URL="/&lt;vilib&gt;/DAQmx/read.llb/DAQmx Read (Analog 1D DBL NChan 1Samp).vi"/>
				<Item Name="DAQmx Read (Analog 1D Wfm NChan 1Samp).vi" Type="VI" URL="/&lt;vilib&gt;/DAQmx/read.llb/DAQmx Read (Analog 1D Wfm NChan 1Samp).vi"/>
				<Item Name="DAQmx Read (Analog 1D Wfm NChan NSamp).vi" Type="VI" URL="/&lt;vilib&gt;/DAQmx/read.llb/DAQmx Read (Analog 1D Wfm NChan NSamp).vi"/>
				<Item Name="DAQmx Read (Analog 2D DBL NChan NSamp).vi" Type="VI" URL="/&lt;vilib&gt;/DAQmx/read.llb/DAQmx Read (Analog 2D DBL NChan NSamp).vi"/>
				<Item Name="DAQmx Read (Analog 2D I16 NChan NSamp).vi" Type="VI" URL="/&lt;vilib&gt;/DAQmx/read.llb/DAQmx Read (Analog 2D I16 NChan NSamp).vi"/>
				<Item Name="DAQmx Read (Analog 2D I32 NChan NSamp).vi" Type="VI" URL="/&lt;vilib&gt;/DAQmx/read.llb/DAQmx Read (Analog 2D I32 NChan NSamp).vi"/>
				<Item Name="DAQmx Read (Analog 2D U16 NChan NSamp).vi" Type="VI" URL="/&lt;vilib&gt;/DAQmx/read.llb/DAQmx Read (Analog 2D U16 NChan NSamp).vi"/>
				<Item Name="DAQmx Read (Analog 2D U32 NChan NSamp).vi" Type="VI" URL="/&lt;vilib&gt;/DAQmx/read.llb/DAQmx Read (Analog 2D U32 NChan NSamp).vi"/>
				<Item Name="DAQmx Read (Analog DBL 1Chan 1Samp).vi" Type="VI" URL="/&lt;vilib&gt;/DAQmx/read.llb/DAQmx Read (Analog DBL 1Chan 1Samp).vi"/>
				<Item Name="DAQmx Read (Analog Wfm 1Chan 1Samp).vi" Type="VI" URL="/&lt;vilib&gt;/DAQmx/read.llb/DAQmx Read (Analog Wfm 1Chan 1Samp).vi"/>
				<Item Name="DAQmx Read (Analog Wfm 1Chan NSamp).vi" Type="VI" URL="/&lt;vilib&gt;/DAQmx/read.llb/DAQmx Read (Analog Wfm 1Chan NSamp).vi"/>
				<Item Name="DAQmx Read (Counter 1D DBL 1Chan NSamp).vi" Type="VI" URL="/&lt;vilib&gt;/DAQmx/read.llb/DAQmx Read (Counter 1D DBL 1Chan NSamp).vi"/>
				<Item Name="DAQmx Read (Counter 1D Pulse Freq 1 Chan NSamp).vi" Type="VI" URL="/&lt;vilib&gt;/DAQmx/read.llb/DAQmx Read (Counter 1D Pulse Freq 1 Chan NSamp).vi"/>
				<Item Name="DAQmx Read (Counter 1D Pulse Ticks 1Chan NSamp).vi" Type="VI" URL="/&lt;vilib&gt;/DAQmx/read.llb/DAQmx Read (Counter 1D Pulse Ticks 1Chan NSamp).vi"/>
				<Item Name="DAQmx Read (Counter 1D Pulse Time 1Chan NSamp).vi" Type="VI" URL="/&lt;vilib&gt;/DAQmx/read.llb/DAQmx Read (Counter 1D Pulse Time 1Chan NSamp).vi"/>
				<Item Name="DAQmx Read (Counter 1D U32 1Chan NSamp).vi" Type="VI" URL="/&lt;vilib&gt;/DAQmx/read.llb/DAQmx Read (Counter 1D U32 1Chan NSamp).vi"/>
				<Item Name="DAQmx Read (Counter DBL 1Chan 1Samp).vi" Type="VI" URL="/&lt;vilib&gt;/DAQmx/read.llb/DAQmx Read (Counter DBL 1Chan 1Samp).vi"/>
				<Item Name="DAQmx Read (Counter Pulse Freq 1 Chan 1 Samp).vi" Type="VI" URL="/&lt;vilib&gt;/DAQmx/read.llb/DAQmx Read (Counter Pulse Freq 1 Chan 1 Samp).vi"/>
				<Item Name="DAQmx Read (Counter Pulse Ticks 1Chan 1Samp).vi" Type="VI" URL="/&lt;vilib&gt;/DAQmx/read.llb/DAQmx Read (Counter Pulse Ticks 1Chan 1Samp).vi"/>
				<Item Name="DAQmx Read (Counter Pulse Time 1Chan 1Samp).vi" Type="VI" URL="/&lt;vilib&gt;/DAQmx/read.llb/DAQmx Read (Counter Pulse Time 1Chan 1Samp).vi"/>
				<Item Name="DAQmx Read (Counter U32 1Chan 1Samp).vi" Type="VI" URL="/&lt;vilib&gt;/DAQmx/read.llb/DAQmx Read (Counter U32 1Chan 1Samp).vi"/>
				<Item Name="DAQmx Read (Digital 1D Bool 1Chan 1Samp).vi" Type="VI" URL="/&lt;vilib&gt;/DAQmx/read.llb/DAQmx Read (Digital 1D Bool 1Chan 1Samp).vi"/>
				<Item Name="DAQmx Read (Digital 1D Bool NChan 1Samp 1Line).vi" Type="VI" URL="/&lt;vilib&gt;/DAQmx/read.llb/DAQmx Read (Digital 1D Bool NChan 1Samp 1Line).vi"/>
				<Item Name="DAQmx Read (Digital 1D U8 1Chan NSamp).vi" Type="VI" URL="/&lt;vilib&gt;/DAQmx/read.llb/DAQmx Read (Digital 1D U8 1Chan NSamp).vi"/>
				<Item Name="DAQmx Read (Digital 1D U8 NChan 1Samp).vi" Type="VI" URL="/&lt;vilib&gt;/DAQmx/read.llb/DAQmx Read (Digital 1D U8 NChan 1Samp).vi"/>
				<Item Name="DAQmx Read (Digital 1D U16 1Chan NSamp).vi" Type="VI" URL="/&lt;vilib&gt;/DAQmx/read.llb/DAQmx Read (Digital 1D U16 1Chan NSamp).vi"/>
				<Item Name="DAQmx Read (Digital 1D U16 NChan 1Samp).vi" Type="VI" URL="/&lt;vilib&gt;/DAQmx/read.llb/DAQmx Read (Digital 1D U16 NChan 1Samp).vi"/>
				<Item Name="DAQmx Read (Digital 1D U32 1Chan NSamp).vi" Type="VI" URL="/&lt;vilib&gt;/DAQmx/read.llb/DAQmx Read (Digital 1D U32 1Chan NSamp).vi"/>
				<Item Name="DAQmx Read (Digital 1D U32 NChan 1Samp).vi" Type="VI" URL="/&lt;vilib&gt;/DAQmx/read.llb/DAQmx Read (Digital 1D U32 NChan 1Samp).vi"/>
				<Item Name="DAQmx Read (Digital 1D Wfm NChan 1Samp).vi" Type="VI" URL="/&lt;vilib&gt;/DAQmx/read.llb/DAQmx Read (Digital 1D Wfm NChan 1Samp).vi"/>
				<Item Name="DAQmx Read (Digital 1D Wfm NChan NSamp).vi" Type="VI" URL="/&lt;vilib&gt;/DAQmx/read.llb/DAQmx Read (Digital 1D Wfm NChan NSamp).vi"/>
				<Item Name="DAQmx Read (Digital 2D Bool NChan 1Samp NLine).vi" Type="VI" URL="/&lt;vilib&gt;/DAQmx/read.llb/DAQmx Read (Digital 2D Bool NChan 1Samp NLine).vi"/>
				<Item Name="DAQmx Read (Digital 2D U8 NChan NSamp).vi" Type="VI" URL="/&lt;vilib&gt;/DAQmx/read.llb/DAQmx Read (Digital 2D U8 NChan NSamp).vi"/>
				<Item Name="DAQmx Read (Digital 2D U16 NChan NSamp).vi" Type="VI" URL="/&lt;vilib&gt;/DAQmx/read.llb/DAQmx Read (Digital 2D U16 NChan NSamp).vi"/>
				<Item Name="DAQmx Read (Digital 2D U32 NChan NSamp).vi" Type="VI" URL="/&lt;vilib&gt;/DAQmx/read.llb/DAQmx Read (Digital 2D U32 NChan NSamp).vi"/>
				<Item Name="DAQmx Read (Digital Bool 1Line 1Point).vi" Type="VI" URL="/&lt;vilib&gt;/DAQmx/read.llb/DAQmx Read (Digital Bool 1Line 1Point).vi"/>
				<Item Name="DAQmx Read (Digital U8 1Chan 1Samp).vi" Type="VI" URL="/&lt;vilib&gt;/DAQmx/read.llb/DAQmx Read (Digital U8 1Chan 1Samp).vi"/>
				<Item Name="DAQmx Read (Digital U16 1Chan 1Samp).vi" Type="VI" URL="/&lt;vilib&gt;/DAQmx/read.llb/DAQmx Read (Digital U16 1Chan 1Samp).vi"/>
				<Item Name="DAQmx Read (Digital U32 1Chan 1Samp).vi" Type="VI" URL="/&lt;vilib&gt;/DAQmx/read.llb/DAQmx Read (Digital U32 1Chan 1Samp).vi"/>
				<Item Name="DAQmx Read (Digital Wfm 1Chan 1Samp).vi" Type="VI" URL="/&lt;vilib&gt;/DAQmx/read.llb/DAQmx Read (Digital Wfm 1Chan 1Samp).vi"/>
				<Item Name="DAQmx Read (Digital Wfm 1Chan NSamp).vi" Type="VI" URL="/&lt;vilib&gt;/DAQmx/read.llb/DAQmx Read (Digital Wfm 1Chan NSamp).vi"/>
				<Item Name="DAQmx Read (Raw 1D I8).vi" Type="VI" URL="/&lt;vilib&gt;/DAQmx/read.llb/DAQmx Read (Raw 1D I8).vi"/>
				<Item Name="DAQmx Read (Raw 1D I16).vi" Type="VI" URL="/&lt;vilib&gt;/DAQmx/read.llb/DAQmx Read (Raw 1D I16).vi"/>
				<Item Name="DAQmx Read (Raw 1D I32).vi" Type="VI" URL="/&lt;vilib&gt;/DAQmx/read.llb/DAQmx Read (Raw 1D I32).vi"/>
				<Item Name="DAQmx Read (Raw 1D U8).vi" Type="VI" URL="/&lt;vilib&gt;/DAQmx/read.llb/DAQmx Read (Raw 1D U8).vi"/>
				<Item Name="DAQmx Read (Raw 1D U16).vi" Type="VI" URL="/&lt;vilib&gt;/DAQmx/read.llb/DAQmx Read (Raw 1D U16).vi"/>
				<Item Name="DAQmx Read (Raw 1D U32).vi" Type="VI" URL="/&lt;vilib&gt;/DAQmx/read.llb/DAQmx Read (Raw 1D U32).vi"/>
				<Item Name="DAQmx Read.vi" Type="VI" URL="/&lt;vilib&gt;/DAQmx/read.llb/DAQmx Read.vi"/>
				<Item Name="DAQmx Reset Device.vi" Type="VI" URL="/&lt;vilib&gt;/DAQmx/configure/system.llb/DAQmx Reset Device.vi"/>
				<Item Name="DAQmx Rollback Channel If Error.vi" Type="VI" URL="/&lt;vilib&gt;/DAQmx/create/channels.llb/DAQmx Rollback Channel If Error.vi"/>
				<Item Name="DAQmx Set CJC Parameters (sub).vi" Type="VI" URL="/&lt;vilib&gt;/DAQmx/create/channels.llb/DAQmx Set CJC Parameters (sub).vi"/>
				<Item Name="DAQmx Start Task.vi" Type="VI" URL="/&lt;vilib&gt;/DAQmx/configure/task.llb/DAQmx Start Task.vi"/>
				<Item Name="DAQmx Timing (Burst Export Clock).vi" Type="VI" URL="/&lt;vilib&gt;/DAQmx/configure/timing.llb/DAQmx Timing (Burst Export Clock).vi"/>
				<Item Name="DAQmx Timing (Burst Import Clock).vi" Type="VI" URL="/&lt;vilib&gt;/DAQmx/configure/timing.llb/DAQmx Timing (Burst Import Clock).vi"/>
				<Item Name="DAQmx Timing (Change Detection).vi" Type="VI" URL="/&lt;vilib&gt;/DAQmx/configure/timing.llb/DAQmx Timing (Change Detection).vi"/>
				<Item Name="DAQmx Timing (Handshaking).vi" Type="VI" URL="/&lt;vilib&gt;/DAQmx/configure/timing.llb/DAQmx Timing (Handshaking).vi"/>
				<Item Name="DAQmx Timing (Implicit).vi" Type="VI" URL="/&lt;vilib&gt;/DAQmx/configure/timing.llb/DAQmx Timing (Implicit).vi"/>
				<Item Name="DAQmx Timing (Pipelined Sample Clock).vi" Type="VI" URL="/&lt;vilib&gt;/DAQmx/configure/timing.llb/DAQmx Timing (Pipelined Sample Clock).vi"/>
				<Item Name="DAQmx Timing (Sample Clock).vi" Type="VI" URL="/&lt;vilib&gt;/DAQmx/configure/timing.llb/DAQmx Timing (Sample Clock).vi"/>
				<Item Name="DAQmx Timing (Use Waveform).vi" Type="VI" URL="/&lt;vilib&gt;/DAQmx/configure/timing.llb/DAQmx Timing (Use Waveform).vi"/>
				<Item Name="DAQmx Timing.vi" Type="VI" URL="/&lt;vilib&gt;/DAQmx/configure/timing.llb/DAQmx Timing.vi"/>
				<Item Name="DAQmx Write (Analog 1D DBL 1Chan NSamp).vi" Type="VI" URL="/&lt;vilib&gt;/DAQmx/write.llb/DAQmx Write (Analog 1D DBL 1Chan NSamp).vi"/>
				<Item Name="DAQmx Write (Analog 1D DBL NChan 1Samp).vi" Type="VI" URL="/&lt;vilib&gt;/DAQmx/write.llb/DAQmx Write (Analog 1D DBL NChan 1Samp).vi"/>
				<Item Name="DAQmx Write (Analog 1D Wfm NChan 1Samp).vi" Type="VI" URL="/&lt;vilib&gt;/DAQmx/write.llb/DAQmx Write (Analog 1D Wfm NChan 1Samp).vi"/>
				<Item Name="DAQmx Write (Analog 1D Wfm NChan NSamp).vi" Type="VI" URL="/&lt;vilib&gt;/DAQmx/write.llb/DAQmx Write (Analog 1D Wfm NChan NSamp).vi"/>
				<Item Name="DAQmx Write (Analog 2D DBL NChan NSamp).vi" Type="VI" URL="/&lt;vilib&gt;/DAQmx/write.llb/DAQmx Write (Analog 2D DBL NChan NSamp).vi"/>
				<Item Name="DAQmx Write (Analog 2D I16 NChan NSamp).vi" Type="VI" URL="/&lt;vilib&gt;/DAQmx/write.llb/DAQmx Write (Analog 2D I16 NChan NSamp).vi"/>
				<Item Name="DAQmx Write (Analog 2D I32 NChan NSamp).vi" Type="VI" URL="/&lt;vilib&gt;/DAQmx/write.llb/DAQmx Write (Analog 2D I32 NChan NSamp).vi"/>
				<Item Name="DAQmx Write (Analog 2D U16 NChan NSamp).vi" Type="VI" URL="/&lt;vilib&gt;/DAQmx/write.llb/DAQmx Write (Analog 2D U16 NChan NSamp).vi"/>
				<Item Name="DAQmx Write (Analog DBL 1Chan 1Samp).vi" Type="VI" URL="/&lt;vilib&gt;/DAQmx/write.llb/DAQmx Write (Analog DBL 1Chan 1Samp).vi"/>
				<Item Name="DAQmx Write (Analog Wfm 1Chan 1Samp).vi" Type="VI" URL="/&lt;vilib&gt;/DAQmx/write.llb/DAQmx Write (Analog Wfm 1Chan 1Samp).vi"/>
				<Item Name="DAQmx Write (Analog Wfm 1Chan NSamp).vi" Type="VI" URL="/&lt;vilib&gt;/DAQmx/write.llb/DAQmx Write (Analog Wfm 1Chan NSamp).vi"/>
				<Item Name="DAQmx Write (Counter 1D Frequency 1Chan NSamp).vi" Type="VI" URL="/&lt;vilib&gt;/DAQmx/write.llb/DAQmx Write (Counter 1D Frequency 1Chan NSamp).vi"/>
				<Item Name="DAQmx Write (Counter 1D Frequency NChan 1Samp).vi" Type="VI" URL="/&lt;vilib&gt;/DAQmx/write.llb/DAQmx Write (Counter 1D Frequency NChan 1Samp).vi"/>
				<Item Name="DAQmx Write (Counter 1D Ticks 1Chan NSamp).vi" Type="VI" URL="/&lt;vilib&gt;/DAQmx/write.llb/DAQmx Write (Counter 1D Ticks 1Chan NSamp).vi"/>
				<Item Name="DAQmx Write (Counter 1D Time 1Chan NSamp).vi" Type="VI" URL="/&lt;vilib&gt;/DAQmx/write.llb/DAQmx Write (Counter 1D Time 1Chan NSamp).vi"/>
				<Item Name="DAQmx Write (Counter 1D Time NChan 1Samp).vi" Type="VI" URL="/&lt;vilib&gt;/DAQmx/write.llb/DAQmx Write (Counter 1D Time NChan 1Samp).vi"/>
				<Item Name="DAQmx Write (Counter 1DTicks NChan 1Samp).vi" Type="VI" URL="/&lt;vilib&gt;/DAQmx/write.llb/DAQmx Write (Counter 1DTicks NChan 1Samp).vi"/>
				<Item Name="DAQmx Write (Counter Frequency 1Chan 1Samp).vi" Type="VI" URL="/&lt;vilib&gt;/DAQmx/write.llb/DAQmx Write (Counter Frequency 1Chan 1Samp).vi"/>
				<Item Name="DAQmx Write (Counter Ticks 1Chan 1Samp).vi" Type="VI" URL="/&lt;vilib&gt;/DAQmx/write.llb/DAQmx Write (Counter Ticks 1Chan 1Samp).vi"/>
				<Item Name="DAQmx Write (Counter Time 1Chan 1Samp).vi" Type="VI" URL="/&lt;vilib&gt;/DAQmx/write.llb/DAQmx Write (Counter Time 1Chan 1Samp).vi"/>
				<Item Name="DAQmx Write (Digital 1D Bool 1Chan 1Samp).vi" Type="VI" URL="/&lt;vilib&gt;/DAQmx/write.llb/DAQmx Write (Digital 1D Bool 1Chan 1Samp).vi"/>
				<Item Name="DAQmx Write (Digital 1D Bool NChan 1Samp 1Line).vi" Type="VI" URL="/&lt;vilib&gt;/DAQmx/write.llb/DAQmx Write (Digital 1D Bool NChan 1Samp 1Line).vi"/>
				<Item Name="DAQmx Write (Digital 1D U8 1Chan NSamp).vi" Type="VI" URL="/&lt;vilib&gt;/DAQmx/write.llb/DAQmx Write (Digital 1D U8 1Chan NSamp).vi"/>
				<Item Name="DAQmx Write (Digital 1D U8 NChan 1Samp).vi" Type="VI" URL="/&lt;vilib&gt;/DAQmx/write.llb/DAQmx Write (Digital 1D U8 NChan 1Samp).vi"/>
				<Item Name="DAQmx Write (Digital 1D U16 1Chan NSamp).vi" Type="VI" URL="/&lt;vilib&gt;/DAQmx/write.llb/DAQmx Write (Digital 1D U16 1Chan NSamp).vi"/>
				<Item Name="DAQmx Write (Digital 1D U16 NChan 1Samp).vi" Type="VI" URL="/&lt;vilib&gt;/DAQmx/write.llb/DAQmx Write (Digital 1D U16 NChan 1Samp).vi"/>
				<Item Name="DAQmx Write (Digital 1D U32 1Chan NSamp).vi" Type="VI" URL="/&lt;vilib&gt;/DAQmx/write.llb/DAQmx Write (Digital 1D U32 1Chan NSamp).vi"/>
				<Item Name="DAQmx Write (Digital 1D U32 NChan 1Samp).vi" Type="VI" URL="/&lt;vilib&gt;/DAQmx/write.llb/DAQmx Write (Digital 1D U32 NChan 1Samp).vi"/>
				<Item Name="DAQmx Write (Digital 1D Wfm NChan 1Samp).vi" Type="VI" URL="/&lt;vilib&gt;/DAQmx/write.llb/DAQmx Write (Digital 1D Wfm NChan 1Samp).vi"/>
				<Item Name="DAQmx Write (Digital 1D Wfm NChan NSamp).vi" Type="VI" URL="/&lt;vilib&gt;/DAQmx/write.llb/DAQmx Write (Digital 1D Wfm NChan NSamp).vi"/>
				<Item Name="DAQmx Write (Digital 2D Bool NChan 1Samp NLine).vi" Type="VI" URL="/&lt;vilib&gt;/DAQmx/write.llb/DAQmx Write (Digital 2D Bool NChan 1Samp NLine).vi"/>
				<Item Name="DAQmx Write (Digital 2D U8 NChan NSamp).vi" Type="VI" URL="/&lt;vilib&gt;/DAQmx/write.llb/DAQmx Write (Digital 2D U8 NChan NSamp).vi"/>
				<Item Name="DAQmx Write (Digital 2D U16 NChan NSamp).vi" Type="VI" URL="/&lt;vilib&gt;/DAQmx/write.llb/DAQmx Write (Digital 2D U16 NChan NSamp).vi"/>
				<Item Name="DAQmx Write (Digital 2D U32 NChan NSamp).vi" Type="VI" URL="/&lt;vilib&gt;/DAQmx/write.llb/DAQmx Write (Digital 2D U32 NChan NSamp).vi"/>
				<Item Name="DAQmx Write (Digital Bool 1Line 1Point).vi" Type="VI" URL="/&lt;vilib&gt;/DAQmx/write.llb/DAQmx Write (Digital Bool 1Line 1Point).vi"/>
				<Item Name="DAQmx Write (Digital U8 1Chan 1Samp).vi" Type="VI" URL="/&lt;vilib&gt;/DAQmx/write.llb/DAQmx Write (Digital U8 1Chan 1Samp).vi"/>
				<Item Name="DAQmx Write (Digital U16 1Chan 1Samp).vi" Type="VI" URL="/&lt;vilib&gt;/DAQmx/write.llb/DAQmx Write (Digital U16 1Chan 1Samp).vi"/>
				<Item Name="DAQmx Write (Digital U32 1Chan 1Samp).vi" Type="VI" URL="/&lt;vilib&gt;/DAQmx/write.llb/DAQmx Write (Digital U32 1Chan 1Samp).vi"/>
				<Item Name="DAQmx Write (Digital Wfm 1Chan 1Samp).vi" Type="VI" URL="/&lt;vilib&gt;/DAQmx/write.llb/DAQmx Write (Digital Wfm 1Chan 1Samp).vi"/>
				<Item Name="DAQmx Write (Digital Wfm 1Chan NSamp).vi" Type="VI" URL="/&lt;vilib&gt;/DAQmx/write.llb/DAQmx Write (Digital Wfm 1Chan NSamp).vi"/>
				<Item Name="DAQmx Write (Raw 1D I8).vi" Type="VI" URL="/&lt;vilib&gt;/DAQmx/write.llb/DAQmx Write (Raw 1D I8).vi"/>
				<Item Name="DAQmx Write (Raw 1D I16).vi" Type="VI" URL="/&lt;vilib&gt;/DAQmx/write.llb/DAQmx Write (Raw 1D I16).vi"/>
				<Item Name="DAQmx Write (Raw 1D I32).vi" Type="VI" URL="/&lt;vilib&gt;/DAQmx/write.llb/DAQmx Write (Raw 1D I32).vi"/>
				<Item Name="DAQmx Write (Raw 1D U8).vi" Type="VI" URL="/&lt;vilib&gt;/DAQmx/write.llb/DAQmx Write (Raw 1D U8).vi"/>
				<Item Name="DAQmx Write (Raw 1D U16).vi" Type="VI" URL="/&lt;vilib&gt;/DAQmx/write.llb/DAQmx Write (Raw 1D U16).vi"/>
				<Item Name="DAQmx Write (Raw 1D U32).vi" Type="VI" URL="/&lt;vilib&gt;/DAQmx/write.llb/DAQmx Write (Raw 1D U32).vi"/>
				<Item Name="DAQmx Write.vi" Type="VI" URL="/&lt;vilib&gt;/DAQmx/write.llb/DAQmx Write.vi"/>
				<Item Name="Destroy ActiveX Event Queue.vi" Type="VI" URL="/&lt;vilib&gt;/Platform/ax-events.llb/Destroy ActiveX Event Queue.vi"/>
				<Item Name="Details Display Dialog.vi" Type="VI" URL="/&lt;vilib&gt;/Utility/error.llb/Details Display Dialog.vi"/>
				<Item Name="DialogType.ctl" Type="VI" URL="/&lt;vilib&gt;/Utility/error.llb/DialogType.ctl"/>
				<Item Name="DialogTypeEnum.ctl" Type="VI" URL="/&lt;vilib&gt;/Utility/error.llb/DialogTypeEnum.ctl"/>
				<Item Name="Directory of Top Level VI.vi" Type="VI" URL="/&lt;vilib&gt;/picture/jpeg.llb/Directory of Top Level VI.vi"/>
				<Item Name="Draw Flattened Pixmap.vi" Type="VI" URL="/&lt;vilib&gt;/picture/picture.llb/Draw Flattened Pixmap.vi"/>
				<Item Name="DTbl Digital Size.vi" Type="VI" URL="/&lt;vilib&gt;/Waveform/DTblOps.llb/DTbl Digital Size.vi"/>
				<Item Name="DTbl Uncompress Digital.vi" Type="VI" URL="/&lt;vilib&gt;/Waveform/DTblOps.llb/DTbl Uncompress Digital.vi"/>
				<Item Name="DWDT Uncompress Digital.vi" Type="VI" URL="/&lt;vilib&gt;/Waveform/DWDTOps.llb/DWDT Uncompress Digital.vi"/>
				<Item Name="Error Cluster From Error Code.vi" Type="VI" URL="/&lt;vilib&gt;/Utility/error.llb/Error Cluster From Error Code.vi"/>
				<Item Name="Error Code Database.vi" Type="VI" URL="/&lt;vilib&gt;/Utility/error.llb/Error Code Database.vi"/>
				<Item Name="ErrWarn.ctl" Type="VI" URL="/&lt;vilib&gt;/Utility/error.llb/ErrWarn.ctl"/>
				<Item Name="Escape Characters for HTTP.vi" Type="VI" URL="/&lt;vilib&gt;/printing/PathToURL.llb/Escape Characters for HTTP.vi"/>
				<Item Name="EventData.ctl" Type="VI" URL="/&lt;vilib&gt;/Platform/ax-events.llb/EventData.ctl"/>
				<Item Name="eventvkey.ctl" Type="VI" URL="/&lt;vilib&gt;/event_ctls.llb/eventvkey.ctl"/>
				<Item Name="Find First Error.vi" Type="VI" URL="/&lt;vilib&gt;/Utility/error.llb/Find First Error.vi"/>
				<Item Name="Find Tag.vi" Type="VI" URL="/&lt;vilib&gt;/Utility/error.llb/Find Tag.vi"/>
				<Item Name="FixBadRect.vi" Type="VI" URL="/&lt;vilib&gt;/picture/pictutil.llb/FixBadRect.vi"/>
				<Item Name="Flip and Pad for Picture Control.vi" Type="VI" URL="/&lt;vilib&gt;/picture/bmp.llb/Flip and Pad for Picture Control.vi"/>
				<Item Name="Format Message String.vi" Type="VI" URL="/&lt;vilib&gt;/Utility/error.llb/Format Message String.vi"/>
				<Item Name="General Error Handler CORE.vi" Type="VI" URL="/&lt;vilib&gt;/Utility/error.llb/General Error Handler CORE.vi"/>
				<Item Name="General Error Handler.vi" Type="VI" URL="/&lt;vilib&gt;/Utility/error.llb/General Error Handler.vi"/>
				<Item Name="Generate Temporary File Path.vi" Type="VI" URL="/&lt;vilib&gt;/Utility/libraryn.llb/Generate Temporary File Path.vi"/>
				<Item Name="Get LV Class Default Value.vi" Type="VI" URL="/&lt;vilib&gt;/Utility/LVClass/Get LV Class Default Value.vi"/>
				<Item Name="Get String Text Bounds.vi" Type="VI" URL="/&lt;vilib&gt;/Utility/error.llb/Get String Text Bounds.vi"/>
				<Item Name="Get Text Rect.vi" Type="VI" URL="/&lt;vilib&gt;/picture/picture.llb/Get Text Rect.vi"/>
				<Item Name="GetHelpDir.vi" Type="VI" URL="/&lt;vilib&gt;/Utility/error.llb/GetHelpDir.vi"/>
				<Item Name="GetRTHostConnectedProp.vi" Type="VI" URL="/&lt;vilib&gt;/Utility/error.llb/GetRTHostConnectedProp.vi"/>
				<Item Name="imagedata.ctl" Type="VI" URL="/&lt;vilib&gt;/picture/picture.llb/imagedata.ctl"/>
				<Item Name="Longest Line Length in Pixels.vi" Type="VI" URL="/&lt;vilib&gt;/Utility/error.llb/Longest Line Length in Pixels.vi"/>
				<Item Name="LV70DateRecToTimeStamp.vi" Type="VI" URL="/&lt;vilib&gt;/_oldvers/_oldvers.llb/LV70DateRecToTimeStamp.vi"/>
				<Item Name="LV70U32ToDateRec.vi" Type="VI" URL="/&lt;vilib&gt;/_oldvers/_oldvers.llb/LV70U32ToDateRec.vi"/>
				<Item Name="LVBoundsTypeDef.ctl" Type="VI" URL="/&lt;vilib&gt;/Utility/miscctls.llb/LVBoundsTypeDef.ctl"/>
				<Item Name="NI_AALBase.lvlib" Type="Library" URL="/&lt;vilib&gt;/Analysis/NI_AALBase.lvlib"/>
				<Item Name="NI_AALPro.lvlib" Type="Library" URL="/&lt;vilib&gt;/Analysis/NI_AALPro.lvlib"/>
				<Item Name="NI_FileType.lvlib" Type="Library" URL="/&lt;vilib&gt;/Utility/lvfile.llb/NI_FileType.lvlib"/>
				<Item Name="NI_HTML.lvclass" Type="LVClass" URL="/&lt;vilib&gt;/Utility/NIReport.llb/HTML/NI_HTML.lvclass"/>
				<Item Name="NI_PackedLibraryUtility.lvlib" Type="Library" URL="/&lt;vilib&gt;/Utility/LVLibp/NI_PackedLibraryUtility.lvlib"/>
				<Item Name="NI_report.lvclass" Type="LVClass" URL="/&lt;vilib&gt;/Utility/NIReport.llb/NI_report.lvclass"/>
				<Item Name="NI_ReportGenerationCore.lvlib" Type="Library" URL="/&lt;vilib&gt;/Utility/NIReport.llb/NI_ReportGenerationCore.lvlib"/>
				<Item Name="NI_Standard Report.lvclass" Type="LVClass" URL="/&lt;vilib&gt;/Utility/NIReport.llb/Standard Report/NI_Standard Report.lvclass"/>
				<Item Name="Not Found Dialog.vi" Type="VI" URL="/&lt;vilib&gt;/Utility/error.llb/Not Found Dialog.vi"/>
				<Item Name="OccFireType.ctl" Type="VI" URL="/&lt;vilib&gt;/Platform/ax-events.llb/OccFireType.ctl"/>
				<Item Name="Open File+.vi" Type="VI" URL="/&lt;vilib&gt;/Utility/file.llb/Open File+.vi"/>
				<Item Name="Open Registry Key.vi" Type="VI" URL="/&lt;vilib&gt;/registry/registry.llb/Open Registry Key.vi"/>
				<Item Name="Open Serial Driver.vi" Type="VI" URL="/&lt;vilib&gt;/Instr/_sersup.llb/Open Serial Driver.vi"/>
				<Item Name="Open_Create_Replace File.vi" Type="VI" URL="/&lt;vilib&gt;/_oldvers/_oldvers.llb/Open_Create_Replace File.vi"/>
				<Item Name="Path to URL.vi" Type="VI" URL="/&lt;vilib&gt;/printing/PathToURL.llb/Path to URL.vi"/>
				<Item Name="Read Characters From File.vi" Type="VI" URL="/&lt;vilib&gt;/Utility/file.llb/Read Characters From File.vi"/>
				<Item Name="Read File+ (string).vi" Type="VI" URL="/&lt;vilib&gt;/Utility/file.llb/Read File+ (string).vi"/>
				<Item Name="Read File+ [SGL].vi" Type="VI" URL="/&lt;vilib&gt;/Utility/file.llb/Read File+ [SGL].vi"/>
				<Item Name="Read From Spreadsheet File (DBL).vi" Type="VI" URL="/&lt;vilib&gt;/Utility/file.llb/Read From Spreadsheet File (DBL).vi"/>
				<Item Name="Read From Spreadsheet File (I64).vi" Type="VI" URL="/&lt;vilib&gt;/Utility/file.llb/Read From Spreadsheet File (I64).vi"/>
				<Item Name="Read From Spreadsheet File (string).vi" Type="VI" URL="/&lt;vilib&gt;/Utility/file.llb/Read From Spreadsheet File (string).vi"/>
				<Item Name="Read From Spreadsheet File.vi" Type="VI" URL="/&lt;vilib&gt;/Utility/file.llb/Read From Spreadsheet File.vi"/>
				<Item Name="Read JPEG File.vi" Type="VI" URL="/&lt;vilib&gt;/picture/jpeg.llb/Read JPEG File.vi"/>
				<Item Name="Read Lines From File.vi" Type="VI" URL="/&lt;vilib&gt;/Utility/file.llb/Read Lines From File.vi"/>
				<Item Name="Read PNG File.vi" Type="VI" URL="/&lt;vilib&gt;/picture/png.llb/Read PNG File.vi"/>
				<Item Name="Read Registry Value DWORD.vi" Type="VI" URL="/&lt;vilib&gt;/registry/registry.llb/Read Registry Value DWORD.vi"/>
				<Item Name="Read Registry Value Simple STR.vi" Type="VI" URL="/&lt;vilib&gt;/registry/registry.llb/Read Registry Value Simple STR.vi"/>
				<Item Name="Read Registry Value Simple U32.vi" Type="VI" URL="/&lt;vilib&gt;/registry/registry.llb/Read Registry Value Simple U32.vi"/>
				<Item Name="Read Registry Value Simple.vi" Type="VI" URL="/&lt;vilib&gt;/registry/registry.llb/Read Registry Value Simple.vi"/>
				<Item Name="Read Registry Value STR.vi" Type="VI" URL="/&lt;vilib&gt;/registry/registry.llb/Read Registry Value STR.vi"/>
				<Item Name="Read Registry Value.vi" Type="VI" URL="/&lt;vilib&gt;/registry/registry.llb/Read Registry Value.vi"/>
				<Item Name="Registry Handle Master.vi" Type="VI" URL="/&lt;vilib&gt;/registry/registry.llb/Registry Handle Master.vi"/>
				<Item Name="Registry refnum.ctl" Type="VI" URL="/&lt;vilib&gt;/registry/registry.llb/Registry refnum.ctl"/>
				<Item Name="Registry RtKey.ctl" Type="VI" URL="/&lt;vilib&gt;/registry/registry.llb/Registry RtKey.ctl"/>
				<Item Name="Registry SAM.ctl" Type="VI" URL="/&lt;vilib&gt;/registry/registry.llb/Registry SAM.ctl"/>
				<Item Name="Registry Simplify Data Type.vi" Type="VI" URL="/&lt;vilib&gt;/registry/registry.llb/Registry Simplify Data Type.vi"/>
				<Item Name="Registry View.ctl" Type="VI" URL="/&lt;vilib&gt;/registry/registry.llb/Registry View.ctl"/>
				<Item Name="Registry WinErr-LVErr.vi" Type="VI" URL="/&lt;vilib&gt;/registry/registry.llb/Registry WinErr-LVErr.vi"/>
				<Item Name="Search and Replace Pattern.vi" Type="VI" URL="/&lt;vilib&gt;/Utility/error.llb/Search and Replace Pattern.vi"/>
				<Item Name="Serial Port Read.vi" Type="VI" URL="/&lt;vilib&gt;/Instr/serial.llb/Serial Port Read.vi"/>
				<Item Name="Serial Port Write.vi" Type="VI" URL="/&lt;vilib&gt;/Instr/serial.llb/Serial Port Write.vi"/>
				<Item Name="serpConfig.vi" Type="VI" URL="/&lt;vilib&gt;/Instr/serial.llb/serpConfig.vi"/>
				<Item Name="Set Bold Text.vi" Type="VI" URL="/&lt;vilib&gt;/Utility/error.llb/Set Bold Text.vi"/>
				<Item Name="Set Cursor (Cursor ID).vi" Type="VI" URL="/&lt;vilib&gt;/Utility/cursorutil.llb/Set Cursor (Cursor ID).vi"/>
				<Item Name="Set Cursor (Icon Pict).vi" Type="VI" URL="/&lt;vilib&gt;/Utility/cursorutil.llb/Set Cursor (Icon Pict).vi"/>
				<Item Name="Set Cursor.vi" Type="VI" URL="/&lt;vilib&gt;/Utility/cursorutil.llb/Set Cursor.vi"/>
				<Item Name="Set String Value.vi" Type="VI" URL="/&lt;vilib&gt;/Utility/error.llb/Set String Value.vi"/>
				<Item Name="STR_ASCII-Unicode.vi" Type="VI" URL="/&lt;vilib&gt;/registry/registry.llb/STR_ASCII-Unicode.vi"/>
				<Item Name="System Exec.vi" Type="VI" URL="/&lt;vilib&gt;/Platform/system.llb/System Exec.vi"/>
				<Item Name="TagReturnType.ctl" Type="VI" URL="/&lt;vilib&gt;/Utility/error.llb/TagReturnType.ctl"/>
				<Item Name="Three Button Dialog CORE.vi" Type="VI" URL="/&lt;vilib&gt;/Utility/error.llb/Three Button Dialog CORE.vi"/>
				<Item Name="Three Button Dialog.vi" Type="VI" URL="/&lt;vilib&gt;/Utility/error.llb/Three Button Dialog.vi"/>
				<Item Name="Trim Whitespace.vi" Type="VI" URL="/&lt;vilib&gt;/Utility/error.llb/Trim Whitespace.vi"/>
				<Item Name="VISA Configure Serial Port" Type="VI" URL="/&lt;vilib&gt;/Instr/_visa.llb/VISA Configure Serial Port"/>
				<Item Name="VISA Configure Serial Port (Instr).vi" Type="VI" URL="/&lt;vilib&gt;/Instr/_visa.llb/VISA Configure Serial Port (Instr).vi"/>
				<Item Name="VISA Configure Serial Port (Serial Instr).vi" Type="VI" URL="/&lt;vilib&gt;/Instr/_visa.llb/VISA Configure Serial Port (Serial Instr).vi"/>
				<Item Name="Wait On ActiveX Event.vi" Type="VI" URL="/&lt;vilib&gt;/Platform/ax-events.llb/Wait On ActiveX Event.vi"/>
				<Item Name="Wait types.ctl" Type="VI" URL="/&lt;vilib&gt;/Platform/ax-events.llb/Wait types.ctl"/>
				<Item Name="whitespace.ctl" Type="VI" URL="/&lt;vilib&gt;/Utility/error.llb/whitespace.ctl"/>
				<Item Name="Write BMP Data To Buffer.vi" Type="VI" URL="/&lt;vilib&gt;/picture/bmp.llb/Write BMP Data To Buffer.vi"/>
				<Item Name="Write BMP Data.vi" Type="VI" URL="/&lt;vilib&gt;/picture/bmp.llb/Write BMP Data.vi"/>
				<Item Name="Write BMP File.vi" Type="VI" URL="/&lt;vilib&gt;/picture/bmp.llb/Write BMP File.vi"/>
				<Item Name="Write Characters To File.vi" Type="VI" URL="/&lt;vilib&gt;/Utility/file.llb/Write Characters To File.vi"/>
				<Item Name="Write File+ (string).vi" Type="VI" URL="/&lt;vilib&gt;/Utility/file.llb/Write File+ (string).vi"/>
				<Item Name="Write File+ [SGL].vi" Type="VI" URL="/&lt;vilib&gt;/Utility/file.llb/Write File+ [SGL].vi"/>
				<Item Name="Write JPEG File.vi" Type="VI" URL="/&lt;vilib&gt;/picture/jpeg.llb/Write JPEG File.vi"/>
				<Item Name="Write PNG File.vi" Type="VI" URL="/&lt;vilib&gt;/picture/png.llb/Write PNG File.vi"/>
			</Item>
			<Item Name="Abbreviated Sensor &amp; Location Names.vi" Type="VI" URL="../Sub VIs/Abbreviated Sensor &amp; Location Names.vi"/>
			<Item Name="Add Alarms to active list.vi" Type="VI" URL="../Sub VIs/Add Alarms to active list.vi"/>
			<Item Name="add task.vi" Type="VI" URL="../Sub VIs/add task.vi"/>
			<Item Name="Advapi32.dll" Type="Document" URL="Advapi32.dll">
				<Property Name="NI.PreserveRelativePath" Type="Bool">true</Property>
			</Item>
			<Item Name="Alarm Configuration Utility.vi" Type="VI" URL="../Sub VIs/Alarm Configuration Utility.vi"/>
			<Item Name="Alarms Enumeration Mapping.vi" Type="VI" URL="../Sub VIs/Alarms Enumeration Mapping.vi"/>
			<Item Name="Alarms file paths.vi" Type="VI" URL="../Sub VIs/Alarms file paths.vi"/>
			<Item Name="Alarms Read Config File to Globals.vi" Type="VI" URL="../Sub VIs/Alarms Read Config File to Globals.vi"/>
			<Item Name="Analyze Line.vi" Type="VI" URL="../Sub VIs/Analyze Line.vi"/>
			<Item Name="Array of Column Clusters.vi" Type="VI" URL="../Sub VIs/Array of Column Clusters.vi"/>
			<Item Name="Array of Enabled Estimator Clusters.vi" Type="VI" URL="../Sub VIs/Array of Enabled Estimator Clusters.vi"/>
			<Item Name="Array of Report Info Clusters.vi" Type="VI" URL="../Sub VIs/Array of Report Info Clusters.vi"/>
			<Item Name="ARTS Default Paths.vi" Type="VI" URL="../../ARTS Rev 1.3.1/arts0202.llb/ARTS Default Paths.vi"/>
			<Item Name="ARTS take 1 sample.vi" Type="VI" URL="../Sub VIs/ARTS take 1 sample.vi"/>
			<Item Name="Autocycle during Flow trigger" Type="VI" URL="../Sub VIs/Autocycle during Flow trigger"/>
			<Item Name="Autocycle during Pressure trigger" Type="VI" URL="../Sub VIs/Autocycle during Pressure trigger"/>
			<Item Name="AvgEndPressureAccuracy.vi" Type="VI" URL="../Sub VIs/AvgEndPressureAccuracy.vi"/>
			<Item Name="BREAK Command.vi" Type="VI" URL="../Sub VIs/BREAK Command.vi"/>
			<Item Name="Build Pressure Autozero String.vi" Type="VI" URL="../Sub VIs/Build Pressure Autozero String.vi"/>
			<Item Name="Build Ventilator Readings String.vi" Type="VI" URL="../Sub VIs/Build Ventilator Readings String.vi"/>
			<Item Name="Calculate Coefficients.vi" Type="VI" URL="../Sub VIs/Calculate Coefficients.vi"/>
			<Item Name="Calculate Gas Flow.vi" Type="VI" URL="../Sub VIs/Calculate Gas Flow.vi"/>
			<Item Name="Calculate InspTime.vi" Type="VI" URL="../Sub VIs/Calculate InspTime.vi"/>
			<Item Name="Calibrate AT-MIO-16XE-50.vi" Type="VI" URL="../Sub VIs/Calibrate AT-MIO-16XE-50.vi"/>
			<Item Name="CALIBRATE Command.vi" Type="VI" URL="../Sub VIs/CALIBRATE Command.vi"/>
			<Item Name="CALIBRATE Constants.vi" Type="VI" URL="../Sub VIs/CALIBRATE Constants.vi"/>
			<Item Name="Capture Alarms.vi" Type="VI" URL="../Sub VIs/Capture Alarms.vi"/>
			<Item Name="Capture Breath Data.vi" Type="VI" URL="../Sub VIs/Capture Breath Data.vi"/>
			<Item Name="Capture Data to Report.vi" Type="VI" URL="../Sub VIs/Capture Data to Report.vi"/>
			<Item Name="Check Flow Direction.vi" Type="VI" URL="../Sub VIs/Check Flow Direction.vi"/>
			<Item Name="Check for repeatable volume.vi" Type="VI" URL="../Sub VIs/Check for repeatable volume.vi"/>
			<Item Name="Check if Header Needed.vi" Type="VI" URL="../Sub VIs/Check if Header Needed.vi"/>
			<Item Name="Check Skip Stability Test .vi" Type="VI" URL="../Sub VIs/Check Skip Stability Test .vi"/>
			<Item Name="CheckCalDate.vi" Type="VI" URL="../Sub VIs/CheckCalDate.vi"/>
			<Item Name="CheckEquip.vi" Type="VI" URL="../Sub VIs/CheckEquip.vi"/>
			<Item Name="Clamp In Range (I32).vi" Type="VI" URL="../Sub VIs/Clamp In Range (I32).vi"/>
			<Item Name="Clamp In Range (SGL).vi" Type="VI" URL="../Sub VIs/Clamp In Range (SGL).vi"/>
			<Item Name="ClearError.vi" Type="VI" URL="../Sub VIs/ClearError.vi"/>
			<Item Name="Close UDP Sockets.vi" Type="VI" URL="../Sub VIs/Close UDP Sockets.vi"/>
			<Item Name="CMU 8405.vi" Type="VI" URL="../Sub VIs/CMU 8405.vi"/>
			<Item Name="CMU Ametec.vi" Type="VI" URL="../Sub VIs/CMU Ametec.vi"/>
			<Item Name="CMU BTM5P350D4C-SQ95.vi" Type="VI" URL="../Sub VIs/CMU BTM5P350D4C-SQ95.vi"/>
			<Item Name="CMU BTM50350D4C-SQ95.vi" Type="VI" URL="../Sub VIs/CMU BTM50350D4C-SQ95.vi"/>
			<Item Name="CMU Lab Tool.vi" Type="VI" URL="../Sub VIs/CMU Lab Tool.vi"/>
			<Item Name="CMU Linear.vi" Type="VI" URL="../Sub VIs/CMU Linear.vi"/>
			<Item Name="CMU Therm-X RTD.vi" Type="VI" URL="../Sub VIs/CMU Therm-X RTD.vi"/>
			<Item Name="CMU Therm-X TC.vi" Type="VI" URL="../Sub VIs/CMU Therm-X TC.vi"/>
			<Item Name="CMU Ventilator Flow.vi" Type="VI" URL="../Sub VIs/CMU Ventilator Flow.vi"/>
			<Item Name="CMUFromDAQMAP.VI" Type="VI" URL="../Sub VIs/CMUFromDAQMAP.VI"/>
			<Item Name="COMMENT Command.vi" Type="VI" URL="../Sub VIs/COMMENT Command.vi"/>
			<Item Name="Compl Cal.vi" Type="VI" URL="../Sub VIs/Compl Cal.vi"/>
			<Item Name="compliance.vi" Type="VI" URL="../Sub VIs/compliance.vi"/>
			<Item Name="Configure DIO.vi" Type="VI" URL="../Sub VIs/Configure DIO.vi"/>
			<Item Name="Constant Seq. Test Options.vi" Type="VI" URL="../Sub VIs/Constant Seq. Test Options.vi"/>
			<Item Name="Convert All RPT To RPV.vi" Type="VI" URL="../Sub VIs/Convert All RPT To RPV.vi"/>
			<Item Name="Copy from Scratch To Report.vi" Type="VI" URL="../Sub VIs/Copy from Scratch To Report.vi"/>
			<Item Name="Count Printed Pages.vi" Type="VI" URL="../Sub VIs/Count Printed Pages.vi"/>
			<Item Name="DAQ globals.vi" Type="VI" URL="../Sub VIs/DAQ globals.vi"/>
			<Item Name="DAQ Reset.vi" Type="VI" URL="../Sub VIs/DAQ Reset.vi"/>
			<Item Name="DAQ to UDP latency utility.vi" Type="VI" URL="../Sub VIs/DAQ to UDP latency utility.vi"/>
			<Item Name="Data Line.vi" Type="VI" URL="../../ARTS Rev 1.3.1/Dans Report Manager VIs/Data Line.vi"/>
			<Item Name="DATASIZE Command.vi" Type="VI" URL="../Sub VIs/DATASIZE Command.vi"/>
			<Item Name="DATASIZE Globals.vi" Type="VI" URL="../Sub VIs/DATASIZE Globals.vi"/>
			<Item Name="delete task.vi" Type="VI" URL="../Sub VIs/delete task.vi"/>
			<Item Name="Delete UnNeeded Lines.vi" Type="VI" URL="../Sub VIs/Delete UnNeeded Lines.vi"/>
			<Item Name="Disable Estimator.vi" Type="VI" URL="../Sub VIs/Disable Estimator.vi"/>
			<Item Name="Dispatch display status.vi" Type="VI" URL="../Sub VIs/Dispatch display status.vi"/>
			<Item Name="Dispatcher Command List.vi" Type="VI" URL="../Sub VIs/Dispatcher Command List.vi"/>
			<Item Name="Dispatcher.vi" Type="VI" URL="../Sub VIs/Dispatcher.vi"/>
			<Item Name="Dynamic Compliance.vi" Type="VI" URL="../Sub VIs/Dynamic Compliance.vi"/>
			<Item Name="Dynamic Resistance.vi" Type="VI" URL="../Sub VIs/Dynamic Resistance.vi"/>
			<Item Name="E-Net globals.vi" Type="VI" URL="../Sub VIs/E-Net globals.vi"/>
			<Item Name="EEPROM Init Global Data.vi" Type="VI" URL="../Sub VIs/EEPROM Init Global Data.vi"/>
			<Item Name="EEPROM Read Data.vi" Type="VI" URL="../Sub VIs/EEPROM Read Data.vi"/>
			<Item Name="EEPROM Send Data.vi" Type="VI" URL="../Sub VIs/EEPROM Send Data.vi"/>
			<Item Name="EndExhalationPressureAccuracy.vi" Type="VI" URL="../Sub VIs/EndExhalationPressureAccuracy.vi"/>
			<Item Name="EndInspirationPressureAccuracy.vi" Type="VI" URL="../Sub VIs/EndInspirationPressureAccuracy.vi"/>
			<Item Name="Ensure Has Test Complete.vi" Type="VI" URL="../Sub VIs/Ensure Has Test Complete.vi"/>
			<Item Name="Equipment List Constants.vi" Type="VI" URL="../Sub VIs/Equipment List Constants.vi"/>
			<Item Name="Equipment List Globals.vi" Type="VI" URL="../Sub VIs/Equipment List Globals.vi"/>
			<Item Name="Equipment List String.vi" Type="VI" URL="../Sub VIs/Equipment List String.vi"/>
			<Item Name="Equipment List.vi" Type="VI" URL="../Sub VIs/Equipment List.vi"/>
			<Item Name="err5000.vi" Type="VI" URL="../Sub VIs/err5000.vi"/>
			<Item Name="Error Handler.vi" Type="VI" URL="../Sub VIs/Error Handler.vi"/>
			<Item Name="ErrorSelectionConstant.vi" Type="VI" URL="../Sub VIs/ErrorSelectionConstant.vi"/>
			<Item Name="ESTIMATE Command.vi" Type="VI" URL="../Sub VIs/ESTIMATE Command.vi"/>
			<Item Name="Estimator Info File.vi" Type="VI" URL="../Sub VIs/Estimator Info File.vi"/>
			<Item Name="Estimator Report Initialization.vi" Type="VI" URL="../Sub VIs/Estimator Report Initialization.vi"/>
			<Item Name="EstimatorConstants.vi" Type="VI" URL="../Sub VIs/EstimatorConstants.vi"/>
			<Item Name="EstimatorMain.vi" Type="VI" URL="../Sub VIs/EstimatorMain.vi"/>
			<Item Name="EstimatorPassFail.vi" Type="VI" URL="../Sub VIs/EstimatorPassFail.vi"/>
			<Item Name="EstimatorStringToStatus.vi" Type="VI" URL="../Sub VIs/EstimatorStringToStatus.vi"/>
			<Item Name="EstimatorUncertainty.vi" Type="VI" URL="../Sub VIs/EstimatorUncertainty.vi"/>
			<Item Name="Evaluate Alarms.vi" Type="VI" URL="../Sub VIs/Evaluate Alarms.vi"/>
			<Item Name="ExhalationPressureFallTime.vi" Type="VI" URL="../Sub VIs/ExhalationPressureFallTime.vi"/>
			<Item Name="ExhalationStability.vi" Type="VI" URL="../Sub VIs/ExhalationStability.vi"/>
			<Item Name="Exhaled Volume Flow.vi" Type="VI" URL="../Sub VIs/Exhaled Volume Flow.vi"/>
			<Item Name="ExhaledTidalVolumeAccuracy.vi" Type="VI" URL="../Sub VIs/ExhaledTidalVolumeAccuracy.vi"/>
			<Item Name="File Create w- Overwrite Prompt.vi" Type="VI" URL="../Sub VIs/File Create w- Overwrite Prompt.vi"/>
			<Item Name="File Open and Seek to End.vi" Type="VI" URL="../Sub VIs/File Open and Seek to End.vi"/>
			<Item Name="File Read.vi" Type="VI" URL="../Sub VIs/File Read.vi"/>
			<Item Name="Fill ENET globals.vi" Type="VI" URL="../Sub VIs/Fill ENET globals.vi"/>
			<Item Name="FillEstimateColumns.vi" Type="VI" URL="../Sub VIs/FillEstimateColumns.vi"/>
			<Item Name="Find Asset Name.vi" Type="VI" URL="../Sub VIs/Find Asset Name.vi"/>
			<Item Name="FindSensorInCommap.VI" Type="VI" URL="../Sub VIs/FindSensorInCommap.VI"/>
			<Item Name="FindSensorInDaqmap.VI" Type="VI" URL="../Sub VIs/FindSensorInDaqmap.VI"/>
			<Item Name="FindThresholdIndex.vi" Type="VI" URL="../Sub VIs/FindThresholdIndex.vi"/>
			<Item Name="FindThresholdWindowIndex.vi" Type="VI" URL="../Sub VIs/FindThresholdWindowIndex.vi"/>
			<Item Name="Fix Bad Format Number.vi" Type="VI" URL="../Sub VIs/Fix Bad Format Number.vi"/>
			<Item Name="Flow Transducer Globals.vi" Type="VI" URL="../Sub VIs/Flow Transducer Globals.vi"/>
			<Item Name="Format [u8] to [u32].vi" Type="VI" URL="../Sub VIs/Format [u8] to [u32].vi"/>
			<Item Name="Format [u32] to [u8].vi" Type="VI" URL="../Sub VIs/Format [u32] to [u8].vi"/>
			<Item Name="Format RPV to Print.vi" Type="VI" URL="../Sub VIs/Format RPV to Print.vi"/>
			<Item Name="Generic Prompt.vi" Type="VI" URL="../Sub VIs/Generic Prompt.vi"/>
			<Item Name="GenericSensorType.vi" Type="VI" URL="../Sub VIs/GenericSensorType.vi"/>
			<Item Name="Get Calibrated Lung Value.vi" Type="VI" URL="../Sub VIs/Get Calibrated Lung Value.vi"/>
			<Item Name="Get DAQ channel list.vi" Type="VI" URL="../Sub VIs/Get DAQ channel list.vi"/>
			<Item Name="Get Data File Date &amp; Time.vi" Type="VI" URL="../Sub VIs/Get Data File Date &amp; Time.vi"/>
			<Item Name="Get Data File Path.vi" Type="VI" URL="../Sub VIs/Get Data File Path.vi"/>
			<Item Name="Get EquipList Name.vi" Type="VI" URL="../Sub VIs/Get EquipList Name.vi"/>
			<Item Name="Get Header-Units String.vi" Type="VI" URL="../Sub VIs/Get Header-Units String.vi"/>
			<Item Name="Get Keyword Type.vi" Type="VI" URL="../Sub VIs/Get Keyword Type.vi"/>
			<Item Name="Get Sensor Names.vi" Type="VI" URL="../Sub VIs/Get Sensor Names.vi"/>
			<Item Name="get task.vi" Type="VI" URL="../Sub VIs/get task.vi"/>
			<Item Name="Get Vent Info.vi" Type="VI" URL="../Sub VIs/Get Vent Info.vi"/>
			<Item Name="Get Ventilator Readings (Manual).vi" Type="VI" URL="../Sub VIs/Get Ventilator Readings (Manual).vi"/>
			<Item Name="Get Ventilator Settings (Manual).vi" Type="VI" URL="../Sub VIs/Get Ventilator Settings (Manual).vi"/>
			<Item Name="GetBreathInfo.vi" Type="VI" URL="../Sub VIs/GetBreathInfo.vi"/>
			<Item Name="GetSensorData.vi" Type="VI" URL="../Sub VIs/GetSensorData.vi"/>
			<Item Name="Global PAVcompl.vi" Type="VI" URL="../Sub VIs/Global PAVcompl.vi"/>
			<Item Name="Globals - task manager.vi" Type="VI" URL="../Sub VIs/Globals - task manager.vi"/>
			<Item Name="GlobalsV.vi" Type="VI" URL="../Sub VIs/GlobalsV.vi"/>
			<Item Name="HEADER Command.vi" Type="VI" URL="../Sub VIs/HEADER Command.vi"/>
			<Item Name="Header-Units to Report.vi" Type="VI" URL="../Sub VIs/Header-Units to Report.vi"/>
			<Item Name="II Box Driver.vi" Type="VI" URL="../Sub VIs/II Box Driver.vi"/>
			<Item Name="Init Alarms Search List.vi" Type="VI" URL="../Sub VIs/Init Alarms Search List.vi"/>
			<Item Name="init exe queue.vi" Type="VI" URL="../Sub VIs/init exe queue.vi"/>
			<Item Name="Initialize Globals.vi" Type="VI" URL="../Sub VIs/Initialize Globals.vi"/>
			<Item Name="Insert Acceptance Criteria.vi" Type="VI" URL="../Sub VIs/Insert Acceptance Criteria.vi"/>
			<Item Name="Insert Semicolons.vi" Type="VI" URL="../Sub VIs/Insert Semicolons.vi"/>
			<Item Name="Inspvolflow.vi" Type="VI" URL="../Sub VIs/Inspvolflow.vi"/>
			<Item Name="IsWOB.vi" Type="VI" URL="../Sub VIs/IsWOB.vi"/>
			<Item Name="Julian Date to Month &amp; Day.vi" Type="VI" URL="../Sub VIs/Julian Date to Month &amp; Day.vi"/>
			<Item Name="Justify string.vi" Type="VI" URL="../Sub VIs/Justify string.vi"/>
			<Item Name="kernel32.dll" Type="Document" URL="kernel32.dll">
				<Property Name="NI.PreserveRelativePath" Type="Bool">true</Property>
			</Item>
			<Item Name="Labtool - trigger-.vi" Type="VI" URL="../Sub VIs/Labtool - trigger-.vi"/>
			<Item Name="Labtool globals.vi" Type="VI" URL="../Sub VIs/Labtool globals.vi"/>
			<Item Name="labtool.vi" Type="VI" URL="../Sub VIs/labtool.vi"/>
			<Item Name="LeakRate.vi" Type="VI" URL="../Sub VIs/LeakRate.vi"/>
			<Item Name="Line Up Header.vi" Type="VI" URL="../Sub VIs/Line Up Header.vi"/>
			<Item Name="Line Wrap.vi" Type="VI" URL="../Sub VIs/Line Wrap.vi"/>
			<Item Name="Load ASCII Data.vi" Type="VI" URL="../Sub VIs/Load ASCII Data.vi"/>
			<Item Name="Load Binary File Display Mode.vi" Type="VI" URL="../Sub VIs/Load Binary File Display Mode.vi"/>
			<Item Name="Low Level Write String.vi" Type="VI" URL="../Sub VIs/Low Level Write String.vi"/>
			<Item Name="Lung Volume Data Entry.vi" Type="VI" URL="../Sub VIs/Lung Volume Data Entry.vi"/>
			<Item Name="Lung Volume Manual Setting.vi" Type="VI" URL="../Sub VIs/Lung Volume Manual Setting.vi"/>
			<Item Name="Lung volume mapping global.vi" Type="VI" URL="../Sub VIs/Lung volume mapping global.vi"/>
			<Item Name="lvanlys.dll" Type="Document" URL="/&lt;resource&gt;/lvanlys.dll"/>
			<Item Name="Manage Reports MAIN.vi" Type="VI" URL="../Sub VIs/Manage Reports MAIN.vi"/>
			<Item Name="MeanCircuitPressureAccuracy.vi" Type="VI" URL="../Sub VIs/MeanCircuitPressureAccuracy.vi"/>
			<Item Name="Misc sensor globals.vi" Type="VI" URL="../Sub VIs/Misc sensor globals.vi"/>
			<Item Name="Misc. sequential globals.vi" Type="VI" URL="../Sub VIs/Misc. sequential globals.vi"/>
			<Item Name="MixStability.vi" Type="VI" URL="../Sub VIs/MixStability.vi"/>
			<Item Name="Modify Footer Page Total.vi" Type="VI" URL="../Sub VIs/Modify Footer Page Total.vi"/>
			<Item Name="New Header.vi" Type="VI" URL="../Sub VIs/New Header.vi"/>
			<Item Name="NewPage.vi" Type="VI" URL="../Sub VIs/NewPage.vi"/>
			<Item Name="NEWPavCalc.vi" Type="VI" URL="../Sub VIs/NEWPavCalc.vi"/>
			<Item Name="nilvaiu.dll" Type="Document" URL="nilvaiu.dll">
				<Property Name="NI.PreserveRelativePath" Type="Bool">true</Property>
			</Item>
			<Item Name="O2 Concentration Manual Setting.vi" Type="VI" URL="../Sub VIs/O2 Concentration Manual Setting.vi"/>
			<Item Name="o2cal.vi" Type="VI" URL="../Sub VIs/o2cal.vi"/>
			<Item Name="Open File No Dialog.vi" Type="VI" URL="../Sub VIs/Open File No Dialog.vi"/>
			<Item Name="Open UDP Sockets.vi" Type="VI" URL="../Sub VIs/Open UDP Sockets.vi"/>
			<Item Name="OxygenSensorAccuracy.vi" Type="VI" URL="../Sub VIs/OxygenSensorAccuracy.vi"/>
			<Item Name="PageConstants.vi" Type="VI" URL="../Sub VIs/PageConstants.vi"/>
			<Item Name="Parse String.vi" Type="VI" URL="../Sub VIs/Parse String.vi"/>
			<Item Name="PAV Compl.vi" Type="VI" URL="../Sub VIs/PAV Compl.vi"/>
			<Item Name="PAV Resist.vi" Type="VI" URL="../Sub VIs/PAV Resist.vi"/>
			<Item Name="PAV WOB.vi" Type="VI" URL="../Sub VIs/PAV WOB.vi"/>
			<Item Name="PBVPeakOvershoot.vi" Type="VI" URL="../Sub VIs/PBVPeakOvershoot.vi"/>
			<Item Name="PBVPressureAccuracy.vi" Type="VI" URL="../Sub VIs/PBVPressureAccuracy.vi"/>
			<Item Name="PBVReliefFlowThruExhValve.vi" Type="VI" URL="../Sub VIs/PBVReliefFlowThruExhValve.vi"/>
			<Item Name="PBVRiseTimeForMaximumFAP.vi" Type="VI" URL="../Sub VIs/PBVRiseTimeForMaximumFAP.vi"/>
			<Item Name="PBVStability.vi" Type="VI" URL="../Sub VIs/PBVStability.vi"/>
			<Item Name="PCVRiseTimeForMinimumFAP.vi" Type="VI" URL="../Sub VIs/PCVRiseTimeForMinimumFAP.vi"/>
			<Item Name="PeakCircuitPressureAccuracy.vi" Type="VI" URL="../Sub VIs/PeakCircuitPressureAccuracy.vi"/>
			<Item Name="PEEPAccuracy.vi" Type="VI" URL="../Sub VIs/PEEPAccuracy.vi"/>
			<Item Name="PEEPHighPressureAccuracy.vi" Type="VI" URL="../Sub VIs/PEEPHighPressureAccuracy.vi"/>
			<Item Name="PPT Init.vi" Type="VI" URL="../Sub VIs/PPT Init.vi"/>
			<Item Name="PPT Read Pressure.vi" Type="VI" URL="../Sub VIs/PPT Read Pressure.vi"/>
			<Item Name="PPT Read.vi" Type="VI" URL="../Sub VIs/PPT Read.vi"/>
			<Item Name="PPT Write.vi" Type="VI" URL="../Sub VIs/PPT Write.vi"/>
			<Item Name="PreCapture Data Initialization.vi" Type="VI" URL="../Sub VIs/PreCapture Data Initialization.vi"/>
			<Item Name="Preprocess Error Log.vi" Type="VI" URL="../Sub VIs/Preprocess Error Log.vi"/>
			<Item Name="Print All Reports.vi" Type="VI" URL="../Sub VIs/Print All Reports.vi"/>
			<Item Name="Print Equipment List.vi" Type="VI" URL="../Sub VIs/Print Equipment List.vi"/>
			<Item Name="Print Labtool Display.vi" Type="VI" URL="../Sub VIs/Print Labtool Display.vi"/>
			<Item Name="Print RPV Report.vi" Type="VI" URL="../Sub VIs/Print RPV Report.vi"/>
			<Item Name="Process Alarm Lines.vi" Type="VI" URL="../Sub VIs/Process Alarm Lines.vi"/>
			<Item Name="Process Dittos.vi" Type="VI" URL="../Sub VIs/Process Dittos.vi"/>
			<Item Name="Process Misc.vi" Type="VI" URL="../Sub VIs/Process Misc.vi"/>
			<Item Name="Process RPT to RPV.vi" Type="VI" URL="../Sub VIs/Process RPT to RPV.vi"/>
			<Item Name="PROMPT Command.vi" Type="VI" URL="../Sub VIs/PROMPT Command.vi"/>
			<Item Name="Prompt.vi" Type="VI" URL="../Sub VIs/Prompt.vi"/>
			<Item Name="PSVRiseTimeForMinimumFAP.vi" Type="VI" URL="../Sub VIs/PSVRiseTimeForMinimumFAP.vi"/>
			<Item Name="Raw Binary Data to Ascii Output.vi" Type="VI" URL="../Sub VIs/Raw Binary Data to Ascii Output.vi"/>
			<Item Name="Read Characters From Arts File.vi" Type="VI" URL="../../ARTS Rev 1.3.1/Dans Report Manager VIs/Read Characters From Arts File.vi"/>
			<Item Name="Read DAQ to UDP latency file.vi" Type="VI" URL="../Sub VIs/Read DAQ to UDP latency file.vi"/>
			<Item Name="ReadIEGlobals.vi" Type="VI" URL="../Sub VIs/ReadIEGlobals.vi"/>
			<Item Name="ReadRaw.vi" Type="VI" URL="../Sub VIs/ReadRaw.vi"/>
			<Item Name="Remove Lung Lines.vi" Type="VI" URL="../Sub VIs/Remove Lung Lines.vi"/>
			<Item Name="Remove Some Spaces From Line.vi" Type="VI" URL="../Sub VIs/Remove Some Spaces From Line.vi"/>
			<Item Name="replay.vi" Type="VI" URL="../Sub VIs/replay.vi"/>
			<Item Name="Report End.vi" Type="VI" URL="../Sub VIs/Report End.vi"/>
			<Item Name="Report File Open and Seek to End.vi" Type="VI" URL="../Sub VIs/Report File Open and Seek to End.vi"/>
			<Item Name="Report Info Update.vi" Type="VI" URL="../Sub VIs/Report Info Update.vi"/>
			<Item Name="Report Scratch FileName.vi" Type="VI" URL="../Sub VIs/Report Scratch FileName.vi"/>
			<Item Name="reset exe queue.vi" Type="VI" URL="../Sub VIs/reset exe queue.vi"/>
			<Item Name="resistance.vi" Type="VI" URL="../Sub VIs/resistance.vi"/>
			<Item Name="Resistor String to Type.vi" Type="VI" URL="../Sub VIs/Resistor String to Type.vi"/>
			<Item Name="Resistor Type to String.vi" Type="VI" URL="../Sub VIs/Resistor Type to String.vi"/>
			<Item Name="RPV to RPX Main.vi" Type="VI" URL="../Sub VIs/RPV to RPX Main.vi"/>
			<Item Name="RPV to RPX.vi" Type="VI" URL="../Sub VIs/RPV to RPX.vi"/>
			<Item Name="Save ASCII Data.vi" Type="VI" URL="../Sub VIs/Save ASCII Data.vi"/>
			<Item Name="Search for I-E transitions.vi" Type="VI" URL="../Sub VIs/Search for I-E transitions.vi"/>
			<Item Name="Search Lungs.vi" Type="VI" URL="../Sub VIs/Search Lungs.vi"/>
			<Item Name="Select and Strip from Array.vi" Type="VI" URL="../Sub VIs/Select and Strip from Array.vi"/>
			<Item Name="Select.ctl" Type="VI" URL="../Sub VIs/Select.ctl"/>
			<Item Name="SensorLocation.vi" Type="VI" URL="../Sub VIs/SensorLocation.vi"/>
			<Item Name="SensorModel.vi" Type="VI" URL="../Sub VIs/SensorModel.vi"/>
			<Item Name="SEQ Test.vi" Type="VI" URL="../Sub VIs/SEQ Test.vi"/>
			<Item Name="Setup Images.vi" Type="VI" URL="../Sub VIs/Setup Images.vi"/>
			<Item Name="SETUP.VI" Type="VI" URL="../Sub VIs/SETUP.VI"/>
			<Item Name="SkipIEHeader.vi" Type="VI" URL="../Sub VIs/SkipIEHeader.vi"/>
			<Item Name="SlpmorBtps.vi" Type="VI" URL="../Sub VIs/SlpmorBtps.vi"/>
			<Item Name="software release.vi" Type="VI" URL="../Sub VIs/software release.vi"/>
			<Item Name="Sort Resistor Array.vi" Type="VI" URL="../Sub VIs/Sort Resistor Array.vi"/>
			<Item Name="Status Log.vi" Type="VI" URL="../Sub VIs/Status Log.vi"/>
			<Item Name="String To All Reports.vi" Type="VI" URL="../Sub VIs/String To All Reports.vi"/>
			<Item Name="StringToReport.vi" Type="VI" URL="../Sub VIs/StringToReport.vi"/>
			<Item Name="Take Atmospheric Pressure Reading.vi" Type="VI" URL="../Sub VIs/Take Atmospheric Pressure Reading.vi"/>
			<Item Name="Tare Differential Pressure Sensors.vi" Type="VI" URL="../Sub VIs/Tare Differential Pressure Sensors.vi"/>
			<Item Name="task manager.vi" Type="VI" URL="../Sub VIs/task manager.vi"/>
			<Item Name="TCP decode header.vi" Type="VI" URL="../Sub VIs/TCP decode header.vi"/>
			<Item Name="TCP format header.vi" Type="VI" URL="../Sub VIs/TCP format header.vi"/>
			<Item Name="TCP format settings.vi" Type="VI" URL="../Sub VIs/TCP format settings.vi"/>
			<Item Name="TCP Send VentSet Queue.vi" Type="VI" URL="../Sub VIs/TCP Send VentSet Queue.vi"/>
			<Item Name="TCP Test Open Socket.vi" Type="VI" URL="../Sub VIs/TCP Test Open Socket.vi"/>
			<Item Name="TEST Command.vi" Type="VI" URL="../Sub VIs/TEST Command.vi"/>
			<Item Name="Test UDP packet reception w-fp.vi" Type="VI" URL="../Sub VIs/Test UDP packet reception w-fp.vi"/>
			<Item Name="TESTCOND Command.vi" Type="VI" URL="../Sub VIs/TESTCOND Command.vi"/>
			<Item Name="TESTCOND Constants.vi" Type="VI" URL="../Sub VIs/TESTCOND Constants.vi"/>
			<Item Name="TESTCOND Globals.vi" Type="VI" URL="../Sub VIs/TESTCOND Globals.vi"/>
			<Item Name="TRIGGER Command.vi" Type="VI" URL="../Sub VIs/TRIGGER Command.vi"/>
			<Item Name="TRIGGER Constants.vi" Type="VI" URL="../Sub VIs/TRIGGER Constants.vi"/>
			<Item Name="Trigger Execute.vi" Type="VI" URL="../Sub VIs/Trigger Execute.vi"/>
			<Item Name="Trigger globals.vi" Type="VI" URL="../Sub VIs/Trigger globals.vi"/>
			<Item Name="Tube Compensation.vi" Type="VI" URL="../Sub VIs/Tube Compensation.vi"/>
			<Item Name="UDP Listen Packets.vi" Type="VI" URL="../Sub VIs/UDP Listen Packets.vi"/>
			<Item Name="UDP Read [U8].vi" Type="VI" URL="../Sub VIs/UDP Read [U8].vi"/>
			<Item Name="UDP Read Output File.vi" Type="VI" URL="../Sub VIs/UDP Read Output File.vi"/>
			<Item Name="UDP Receive Alarms Data.vi" Type="VI" URL="../Sub VIs/UDP Receive Alarms Data.vi"/>
			<Item Name="UDP Receive Patient Data.vi" Type="VI" URL="../Sub VIs/UDP Receive Patient Data.vi"/>
			<Item Name="UNITS Command.vi" Type="VI" URL="../Sub VIs/UNITS Command.vi"/>
			<Item Name="Update Column Cluster Value.vi" Type="VI" URL="../Sub VIs/Update Column Cluster Value.vi"/>
			<Item Name="Update Results Counts.vi" Type="VI" URL="../Sub VIs/Update Results Counts.vi"/>
			<Item Name="utilities.vi" Type="VI" URL="../Sub VIs/utilities.vi"/>
			<Item Name="VCVDeliveredOxygenAccuracy.vi" Type="VI" URL="../Sub VIs/VCVDeliveredOxygenAccuracy.vi"/>
			<Item Name="VCVFlowControlAccuracy.vi" Type="VI" URL="../Sub VIs/VCVFlowControlAccuracy.vi"/>
			<Item Name="VCVFlowStability" Type="VI" URL="../Sub VIs/VCVFlowStability"/>
			<Item Name="VCVVolumeAccuracy.vi" Type="VI" URL="../Sub VIs/VCVVolumeAccuracy.vi"/>
			<Item Name="VENTGET Globals.vi" Type="VI" URL="../Sub VIs/VENTGET Globals.vi"/>
			<Item Name="ventilator id.vi" Type="VI" URL="../Sub VIs/ventilator id.vi"/>
			<Item Name="VENTSET Command.vi" Type="VI" URL="../Sub VIs/VENTSET Command.vi"/>
			<Item Name="VENTSET Constants.vi" Type="VI" URL="../Sub VIs/VENTSET Constants.vi"/>
			<Item Name="VENTSET execute command.vi" Type="VI" URL="../Sub VIs/VENTSET execute command.vi"/>
			<Item Name="VENTSET Globals.vi" Type="VI" URL="../Sub VIs/VENTSET Globals.vi"/>
			<Item Name="VNTIDFIL.VI" Type="VI" URL="../Sub VIs/VNTIDFIL.VI"/>
			<Item Name="VolumeCalculation.vi" Type="VI" URL="../Sub VIs/VolumeCalculation.vi"/>
			<Item Name="VolumeFromSLPM.vi" Type="VI" URL="../Sub VIs/VolumeFromSLPM.vi"/>
			<Item Name="WAIT Command.vi" Type="VI" URL="../Sub VIs/WAIT Command.vi"/>
			<Item Name="WAIT Dialog.vi" Type="VI" URL="../Sub VIs/WAIT Dialog.vi"/>
			<Item Name="wait(ms) if no error.vi" Type="VI" URL="../Sub VIs/wait(ms) if no error.vi"/>
			<Item Name="Warning.vi" Type="VI" URL="../Sub VIs/Warning.vi"/>
			<Item Name="Write Characters To ARTS File.vi" Type="VI" URL="../Sub VIs/Write Characters To ARTS File.vi"/>
			<Item Name="Zero Array of Column Clusters.vi" Type="VI" URL="../Sub VIs/Zero Array of Column Clusters.vi"/>
			<Item Name="Zero Array of EEPROM data.vi" Type="VI" URL="../Sub VIs/Zero Array of EEPROM data.vi"/>
			<Item Name="Zero Array of Enabled Estimators.vi" Type="VI" URL="../Sub VIs/Zero Array of Enabled Estimators.vi"/>
			<Item Name="Zero Array of Lung Volumes.vi" Type="VI" URL="../Sub VIs/Zero Array of Lung Volumes.vi"/>
			<Item Name="Zero Array of Report Info Clusters.vi" Type="VI" URL="../Sub VIs/Zero Array of Report Info Clusters.vi"/>
			<Item Name="Zero Autozero Array.vi" Type="VI" URL="../Sub VIs/Zero Autozero Array.vi"/>
			<Item Name="Zero DaqMap, ComMap.vi" Type="VI" URL="../Sub VIs/Zero DaqMap, ComMap.vi"/>
			<Item Name="Zero Trigger Array.vi" Type="VI" URL="../Sub VIs/Zero Trigger Array.vi"/>
			<Item Name="Zero Ventset Queue.vi" Type="VI" URL="../Sub VIs/Zero Ventset Queue.vi"/>
		</Item>
		<Item Name="Build Specifications" Type="Build"/>
	</Item>
</Project>
